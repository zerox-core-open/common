using Autofac;
using Microsoft.Extensions.Logging;
using System;
using Xunit;
using Zerox.Core.Autofac;
using Zerox.Core.Autofac.Dummy;
using Zerox.Core.XUnit;

namespace Zerox.Core.Tests
{
    public class SomeSvc : ISomeSvc, IAutowirable
    {
        public ILogger Logger { get; init; }

        public SomeSvc(ILogger<SomeSvc> l)
        {
            Logger = l;
        }

        public string GetText()
        {
            Logger.LogWarning("SomeSvc:GetText() Called!");
            //if (Logger.IsEnabled(LogLevel.Trace))
            //{
            //    Logger.LogTrace("Some trace.");
            //}
            return "SomeSvc";
        }

        public void PostAutowire()
        {
        }
    }

    public interface ISomeEntity
    {
        string GetText();

        bool Autowired { get; }
    }

    public class SomeEntity : ISomeEntity, IAutowirable
    {
        [Autowired]
        private readonly ISomeSvc _someSvc = default;

        public bool Autowired { get; set; } = false;

        public string GetText()
        {
            return _someSvc.GetText();
        }

        public void PostAutowire()
        {
            Autowired = true;
        }
    }

    // this should register also from zerox.* assemblies
    [AutofacModulePrefixes("Zerox")]
    public class GlobalFixture : XUnitTestingContextBase
    {
        protected override void RegisterMocks(ContainerBuilder builder)
        {
            // this should override!!!! original from core
            builder.RegisterType<SomeSvc>().As<ISomeSvc>().SingleInstance();
            builder.RegisterType<SomeEntity>().AsSelf().As<ISomeEntity>().FieldsAsAutowired().InstancePerDependency();
        }
    }

    [CollectionDefinition("Zerox.Core.Tests")]
    public class ZeroxCoreTestsCollection : ICollectionFixture<GlobalFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }

    [Collection("Zerox.Core.Tests")]
    public class TestingContextTests
    {
        private GlobalFixture _fixture;

        private ILogger Logger { get; init; }

        // this will inject fixture
        public TestingContextTests(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<TestingContextTests>>();
        }

        [Fact]
        public void TestAutofacStart()
        {
            Logger.LogError(new Exception("Test"), "Some exception");

            var fromHere = _fixture.Resolve<ISomeEntity>();
            Assert.NotNull(fromHere);
            Assert.True(fromHere.Autowired);
            Assert.True(fromHere.GetText() == "SomeSvc");

            { // interface
                var fromCore = _fixture.Resolve<IDummyComponent>();
                Assert.NotNull(fromCore);
                Assert.True(fromCore.Autowired);
                Assert.True(fromCore.GetText() == "SomeSvc");
            }

            { // named
                var fromCore = _fixture.ResolveNamed<IDummyComponent>("Dummy");
                Assert.NotNull(fromCore);
                Assert.True(fromCore.Autowired);
                Assert.True(fromCore.GetText() == "SomeSvc");
            }

            { // self
                var fromCore = _fixture.Resolve<DummyComponent>();
                Assert.NotNull(fromCore);
                Assert.True(fromCore.Autowired);
                Assert.True(fromCore.GetText() == "SomeSvc");
            }

            { // MS extensions
                var fromCore = (IDummyComponent)_fixture.ServiceProvider.GetService(typeof(IDummyComponent));
                Assert.NotNull(fromCore);
                Assert.True(fromCore.Autowired);
                Assert.True(fromCore.GetText() == "SomeSvc");
            }
        }
    }
}