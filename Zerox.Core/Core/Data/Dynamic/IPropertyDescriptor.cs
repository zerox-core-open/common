﻿using System;

namespace Zerox.Core.Data.Dynamic
{
    public interface IPropertyMeta
    {
        string ColumnName { get; set; }
        string FieldName { get; set; }
        bool IsIdentity { get; set; }
        bool IsNullable { get; set; }
        bool IsPrimaryKey { get; set; }
        bool IsReadOnly { get; set; }
        bool IsRequired { get; set; }
        int MaxLengt { get; set; }
        int Precision { get; set; }
        int Scale { get; set; }
        bool HasConstrain { get; set; }
        string ConstrainItemId { get; set; }
        string ConstrainFilter { get; set; }
    }

    public interface IPropertyDescriptor : IPropertyMeta
    {
        Type FieldType { get; set; }
    }
}