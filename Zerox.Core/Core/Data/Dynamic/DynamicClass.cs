﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Zerox.Core.Data.Dynamic
{
    /// <summary>
    /// Class representing the meta-information to build a dynamic class
    /// </summary>
    public class DynamicClass : IHasAttributes
    {
        public static ValueHelper Helper { get; } = new ValueHelper();

        public const string TAB = "\t";
        public const string NL = "\r\n";

        public DynamicClass(string name, string? @namespace)
        {
            Name = name;
            Namespace = @namespace;
        }

        /// <summary>
        /// Name of the dynamic class
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// class namespace
        /// </summary>
        public string? Namespace { get; init; }

        /// <summary>
        /// Type full name
        /// </summary>
        public string FullName => $"{Namespace}.{Name}";

        /// <summary>
        /// All usings
        /// </summary>
        public HashSet<Type> Usings { get; } = new HashSet<Type>();

        /// <summary>
        /// Static type that this class will inherit from or implement
        /// </summary>
        public HashSet<Type> Implements { get; } = new HashSet<Type>();

        /// <summary>
        /// Class attributes
        /// </summary>
        public HashSet<DynamicAttrib> Attributes { get; } = new HashSet<DynamicAttrib>();

        /// <summary>
        /// Adds type for implementing
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public DynamicClass Implement(Type t)
        {
            Implements.Add(t);
            return this;
        }

        public DynamicClass Implement<T>()
        {
            return Implement(typeof(T));
        }

        /// <summary>
        /// Adds type for implementing
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public DynamicClass UsingFrom(Type t)
        {
            Usings.Add(t);
            return this;
        }

        public DynamicClass UsingFrom<T>()
        {
            return UsingFrom(typeof(T));
        }

        /// <summary>
        /// Field collection
        /// </summary>
        public HashSet<DynamicProperty> Properties { get; } = new HashSet<DynamicProperty>();

        /// <summary>
        /// Adds property
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public DynamicClass WithProperty(DynamicProperty p)
        {
            Properties.Add(p);
            return this;
        }

        public DynamicClass WithProperty<T>(string name)
        {
            return WithProperty(new DynamicProperty(typeof(T), name, false));
        }

        public DynamicClass WithProperty(Type type, string propertyName, string columnName)
        {
            return WithProperty(DynamicProperty.From(type, propertyName, columnName));
        }

        public DynamicClass WithProperty<T>(string name, Func<DynamicProperty, DynamicProperty> propBuilder)
        {
            return WithProperty(propBuilder(new DynamicProperty(typeof(T), name, false)));
        }

        public DynamicClass WithProperty(Type type, string name)
        {
            return WithProperty(new DynamicProperty(type, name, false));
        }

        /// <summary>
        /// Method collection
        /// </summary>
        public HashSet<DynamicMethod> Methods { get; } = new HashSet<DynamicMethod>();

        public DynamicClass WithMethod(DynamicMethod m)
        {
            Methods.Add(m);
            return this;
        }

        private string TypeToString(Type t)
        {
            return DynamicClass.Helper.Reference(t);
            /*
            if (t.IsGenericType)
            {
                return $"{t.Name.Split('`')[0]}<{string.Join(", ", t.GenericTypeArguments.Select(t => TypeToString(t)))}>";
            }
            else
            {
                return t.Name;
            }
            */
        }

        private void ExtractNamespaces(Type t, ref HashSet<string> usings)
        {
            if (t.IsGenericType)
            {
                usings.Add(t.Namespace ?? "");
                foreach (var tt in t.GenericTypeArguments)
                {
                    ExtractNamespaces(tt, ref usings);
                }
            }
            else
            {
                usings.Add(t.Namespace ?? "");
            }
        }

        /// <summary>
        /// Build the class source code so it can be compiled dynamically later
        /// </summary>
        /// <returns>C# Source code of the class</returns>
        public override string ToString()
        {
            HashSet<string> usgs = new HashSet<string>();

            // gather usings
            foreach (var a in Attributes) usgs.Add(a.Using ?? "");
            foreach (var i in Implements) ExtractNamespaces(i, ref usgs);
            foreach (var p in Properties)
            {
                usgs.Add(p.Using ?? "");
                foreach (var at in p.Attributes) usgs.Add(at.Using ?? "");
            }
            foreach (var u in Usings) ExtractNamespaces(u, ref usgs);

            if (Namespace != default)
            {
                usgs.Remove(Namespace);
            }

            
            return
            $@"{string.Join(NL, usgs.OrderBy(u => u).Where(u => u.Length > 0).Select(u => $"using {u};"))}

namespace {Namespace} {{
{TAB}{string.Join($"{NL}{TAB}", Attributes)}
{TAB}public class {Name}{(Implements.Count > 0 ? $" : {string.Join(",", Implements.Select(i => TypeToString(i)))}" : "")}
{TAB}{{
{TAB}{TAB}{string.Join($"{NL}{NL}{TAB}{TAB}", Properties)}
{TAB}{TAB}{string.Join($"{NL}{NL}{TAB}{TAB}", Methods)}
{TAB}}}
}}";
            
        }
    }
}