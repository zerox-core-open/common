﻿using System.Collections.Generic;

namespace Zerox.Core.Data.Dynamic
{
    public interface IHasAttributes
    {
        HashSet<DynamicAttrib> Attributes { get; }
    }
}