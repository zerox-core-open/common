﻿namespace Zerox.Core.Data.Dynamic
{
    public interface INamedRepositoryData
    {
        string Name { get; }
    }
}