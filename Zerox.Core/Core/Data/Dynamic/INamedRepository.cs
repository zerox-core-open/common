﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Zerox.Core.Data.Dynamic
{
    public interface INamedRepository<T>
        where T : INamedRepositoryData
    {
        // maby not good idea expose it
        // void Add(T d);
        // void Remove(string name);

        // exposed
        bool TryGetData(string name, [MaybeNullWhen(false)] out T data);

        int Size { get; }

        IEnumerable<T> Repo { get; }
    }
}