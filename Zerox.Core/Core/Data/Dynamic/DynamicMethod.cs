﻿using System.Collections.Generic;

namespace Zerox.Core.Data.Dynamic
{
    public class DynamicMethod
    {
        public const string TAB = "\t";
        public const string NL = "\r\n";

        public DynamicMethod(string signature, IEnumerable<string> body)
        {
            Signature = signature;
            Body = body;
        }

        public string Signature { get; init; }
        public IEnumerable<string> Body { get; init; }

        public override string ToString()
        {
            return $"{Signature}{NL}{TAB}{TAB}{{{NL}{TAB}{TAB}{TAB}{string.Join($"{NL}{TAB}{TAB}{TAB}", Body)}{NL}{TAB}{TAB}}}";
        }
    }
}