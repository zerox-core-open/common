﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Zerox.Core.Data.Dynamic
{
    public class NamedRepository<T> : INamedRepository<T>
        where T : INamedRepositoryData
    {
        private Dictionary<string, T> _repo = new Dictionary<string, T>();

        public int Size => _repo.Count;

        public IEnumerable<T> Repo => _repo.Values;

        public void Add(T d)
        {
            _repo[d.Name] = d;
        }

        public void Remove(string name)
        {
            _repo.Remove(name);
        }

        public bool TryGetData(string name, [MaybeNullWhen(false)] out T data)
        {
            return _repo.TryGetValue(name, out data);
        }
    }
}