﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Zerox.Core.Data.Dynamic
{
    public class DynamicAttrib<T> : DynamicAttrib
    {
        public DynamicAttrib() : base(typeof(T))
        {
        }

        public DynamicAttrib(IEnumerable<(string?, object)>? @params) : base(typeof(T), @params)
        {
        }
    }

    public class DynamicAttrib
    {
        public static DynamicAttrib From<T>()
        {
            return new DynamicAttrib<T>();
        }

        public static DynamicAttrib From<T>(params object[] @params)
        {
            return new DynamicAttrib<T>(@params.Select(p => new Tuple<string?, object>(default, p).ToValueTuple()));
        }

        public static DynamicAttrib From<T>(IEnumerable<(string?, object)> @params)
        {
            return new DynamicAttrib<T>(@params);
        }

        public DynamicAttrib(Type type) : this(type, default)
        {
        }

        public DynamicAttrib(Type type, IEnumerable<(string?, object)>? @params)
        {
            Type = type;
            Params = @params;
        }

        public Type Type { get; init; }
        public IEnumerable<(string?, object)>? Params { get; init; }

        public override string ToString()
        {
            if (typeof(Attribute).IsAssignableFrom(Type))
            {
                string pms = "";

                if (Params != default)
                {
                    var vals = Params.Select(p =>
                    {
                        string pfx = p.Item1 == default ? "" : $"{p.Item1} = ";
                        //string q = p.Item2 is string ? "\"" : "";
                        //return $"{pfx}{q}{(p.Item2 is bool ? p.Item2.ToString().ToLower() : p.Item2)}{q}";
                        return $"{pfx}{DynamicClass.Helper.UnknownLiteral(p.Item2)}";
                    });

                    pms = $"({string.Join(", ", vals)})";
                }
                return $"[{Type.Name.Replace("Attribute", "")}{pms}]";
            }
            return "";
        }

        public string? Using => Type.Namespace;
    }
}