﻿using System.Collections.Generic;

namespace Zerox.Core.Data.Dynamic
{
    public static class IHasAttributesExtensions
    {
        public static T WithAttribute<T>(this T self, DynamicAttrib att)
            where T : IHasAttributes
        {
            self.Attributes.Add(att);
            return self;
        }

        public static T WithAttribute<T, AT>(this T self, List<(string?, object)>? @params)
            where T : IHasAttributes
        {
            return WithAttribute(self, new DynamicAttrib<AT>(@params));
        }
    }
}