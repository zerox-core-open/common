﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Zerox.Core.Data.Dynamic
{
    public class DynamicProperty<T> : DynamicProperty
    {
        public DynamicProperty(string name) : base(typeof(T), name, false)
        {
        }
    }

    public class DynamicProperty : IHasAttributes
    {
        public const string TAB = "\t\t";
        public const string NL = "\r\n";

        public HashSet<DynamicAttrib> Attributes { get; } = new HashSet<DynamicAttrib>();

        public DynamicProperty(Type type, string name, bool @override) : this(DynamicClass.Helper.Reference(type), type.Namespace, name, @override)
        {
        }

        public DynamicProperty(string type, string? @namespace, string name, bool @override)
        {
            Name = name;
            PropType = type;
            this.@override = @override;
            Namespace = @namespace;
        }

        public string Name { get; init; }
        public string PropType { get; init; }
        public string? Namespace { get; init; }
        public bool @override { get; protected set; }
        public bool HasBckField { get; protected set; } = false;
        public Func<string, string>? GetSetRenderer { get; protected set; } = default;



        public override string ToString()
        {
            bool nullable = !Attributes.Any(a => typeof(RequiredAttribute).IsAssignableFrom(a.Type) || typeof(KeyAttribute).IsAssignableFrom(a.Type));
            bool sep = Attributes.Count > 0;

            string bf = "";
            if (HasBckField)
            {
                bf = $"private {PropType}{(nullable ? "?" : "")} _{Name};{NL}{TAB}";

                // default renderer
                if(GetSetRenderer == default)
                {
                    GetSetRenderer = (p) => $"{{ get => {p}; set => {p} = value; }}";
                }

            }

            string getSet = "{ get; set; }";

            if(GetSetRenderer != default)
            {
                getSet = GetSetRenderer($"_{Name}");
            }


            return $"{bf}{string.Join($"{NL}{TAB}", Attributes)}{(sep ? $"{NL}{TAB}" : "")}public{(@override ? " override " : " ")}{PropType}{(nullable ? "?" : "")} {Name} {getSet}";
        }

        public string? Using => Namespace;

        public static DynamicProperty From(string type, string? @namespace, string propertyName, string columnName, bool addColumnAttribute = true)
        {
            if (addColumnAttribute)
            {
                return new DynamicProperty(type, @namespace, propertyName, false)
                             .WithAttribute(DynamicAttrib.From<ColumnAttribute>(columnName));
            } else
            {
                return new DynamicProperty(type, @namespace, propertyName, false);
            }
        }

        public static DynamicProperty From(Type type, string propertyName, string columnName)
        {
            return new DynamicProperty(type, propertyName, false)
                         .WithAttribute(DynamicAttrib.From<ColumnAttribute>(columnName));
        }

        public static DynamicProperty From(Type type, string propertyName, string columnName, string columnDbType)
        {
            return new DynamicProperty(type, propertyName, false)
                         .WithAttribute(DynamicAttrib.From<ColumnAttribute>(
                             new List<(string?, object)>() {
                                 (default, columnName),
                                 ("TypeName", columnDbType)
                             }
                         ));
        }

        public static DynamicProperty From(IPropertyDescriptor pd)
        {
            if (pd.FieldType.Name == typeof(DateTime).Name)
            {
                return new DynamicProperty(pd.FieldType, pd.FieldName, false)
                         .WithAttribute(DynamicAttrib.From<ColumnAttribute>(
                             new List<(string?, object)>() {
                                 (default, pd.ColumnName),
                                 ("TypeName", "datetime")
                             }
                         ));
            }
            else if(pd.FieldType.Name == typeof(Decimal).Name)
            {
                return new DynamicProperty(pd.FieldType, pd.FieldName, false)
                         .WithAttribute(DynamicAttrib.From<ColumnAttribute>(
                             new List<(string?, object)>() {
                                 (default, pd.ColumnName),
                                 ("TypeName", $"decimal({pd.Precision},{pd.Scale})")
                             }
                         ));
            } else
            {
                return new DynamicProperty(pd.FieldType, pd.FieldName, false)
                         .WithAttribute(DynamicAttrib.From<ColumnAttribute>(pd.ColumnName));
            }
            
        }

        public DynamicProperty WithOverride()
        {
            this.@override = true;
            return this;
        }

        public DynamicProperty WithBackingField()
        {
            this.HasBckField = true;
            return this;
        }

        public DynamicProperty WithGetSetRenderer(Func<string, string> rnd)
        {
            this.HasBckField = true;
            this.GetSetRenderer = rnd;
            return this;
        }

        /* With custom getter setter */
    }
}