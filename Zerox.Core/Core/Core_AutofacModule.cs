﻿using Autofac;
using MongoDB.Bson;
using System.ComponentModel;
using Zerox.Core.Autofac;
using Zerox.Core.Autofac.Dummy;
using Zerox.Core.CSharp;
using Zerox.Core.Json;

namespace Zerox.Core
{
    public class Core_AutofacModule : ModuleExtended
    {
        protected override void RegisterTypes(ContainerBuilder builder)
        {
            /* 
             * TypeDescriptor converter for ObjectId <-> string
             * DevExtreme.AspNet.Data.Utils when do conversion of Expression need eventual converter if property type is ObjectId
             */
            TypeDescriptor.AddAttributes(typeof(ObjectId), new TypeConverterAttribute(typeof(ExpressionTypeObjectIdConverter)));

            /* Roslyn compiler */
            builder.RegisterType<DynAssembly>().AsSelf().SingleInstance();

            /* TESTING */
            builder.RegisterType<WillBeChangedForTestSvc>().As<ISomeSvc>().SingleInstance();
            builder.RegisterType<DummyComponent>()
                .AsSelf()
                .Named<IDummyComponent>("Dummy")
                .As<IDummyComponent>().FieldsAsAutowired().InstancePerDependency();
        }
    }
}