﻿using Autofac;
using Autofac.Builder;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Zerox.Core.App;

namespace Zerox.Core.Autofac
{

    public static class AutofacExtensions
    {   
        // key for parsed settings file saved in autofac context properties
        public static string APPSETTINGS_PROPERTY_KEY => AutofacExtensionsBase.APPSETTINGS_PROPERTY_KEY;

        public static string[] TryToGetAutofacPrefixes(this Type dest)
        {
            return AutofacExtensionsBase.TryToGetAutofacPrefixes(dest);
        }

        public static ContainerBuilder RegisterConfigFile(this ContainerBuilder builder, IConfiguration cfg)
        {
            return AutofacExtensionsBase.RegisterConfigFile(builder, cfg);
        }

        public static ContainerBuilder RegisterCustoms(this ContainerBuilder builder, Action<ContainerBuilder> callBack)
        {
            return AutofacExtensionsBase.RegisterCustoms(builder, callBack);
        }

        public static ContainerBuilder RegisterModules(this ContainerBuilder builder, string[] assemblyPrefixes, bool doAutomapperProfiles = true)
        {
            if (AutofacExtensionsBase.AssemblyLoader == null) {
                AutofacExtensionsBase.AssemblyLoader = (p) =>
                {
                    // this is not working on .Net Core project see: http://codebuckets.com/2020/05/29/dynamically-loading-assemblies-for-dependency-injection-in-net-core/
                    // return Assembly.LoadFrom(p);
                    return System.Runtime.Loader.AssemblyLoadContext.Default.LoadFromAssemblyPath(p);
                };
            }

            if (doAutomapperProfiles)
            {
                return AutofacExtensionsBase.RegisterModules(builder, assemblyPrefixes, (b, lst) => {
                    // here you have to pass in the assembly (or assemblies) containing AutoMapper types
                    builder.RegisterAutoMapper(true, lst);
                });
            } else
            {
                return AutofacExtensionsBase.RegisterModules(builder, assemblyPrefixes);
            }
        }

       

        public static void InjectFields(IComponentContext context, object? instance, bool overrideSetValues)
        {
            AutofacExtensionsBase.InjectFields(context, instance, overrideSetValues);
        }

        public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> FieldsAsAutowired<TLimit, TActivatorData, TRegistrationStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registration)
        {
            return AutofacExtensionsBase.FieldsAsAutowired(registration);
        }
    }
}