﻿using Microsoft.AspNetCore.Http;

namespace Zerox.Core.Io
{

    public interface IFileDataDto<DataT> : IStreamWithData<DataT>
        where DataT : class, new()
    {
        IFormFile? File { get; set; }
    }
}
