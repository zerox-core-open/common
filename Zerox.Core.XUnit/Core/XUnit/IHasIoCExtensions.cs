﻿using Autofac;
using Autofac.Core;

namespace Zerox.Core.XUnit
{
    public static class IHasIoCExtensions
    {
        public static TEntity Resolve<TEntity>(this IHasIoC self, params Parameter[] parameters)
            where TEntity: notnull
        {
            return self.IoC.Resolve<TEntity>(parameters);
        }

        public static TEntity ResolveNamed<TEntity>(this IHasIoC self, string name, params Parameter[] parameters)
            where TEntity : notnull
        {
            return self.IoC.ResolveNamed<TEntity>(name, parameters);
        }

        public static TEntity Resolve<TEntity>(this IHasIoC self)
            where TEntity : notnull
        {
            return self.IoC.Resolve<TEntity>();
        }

        public static TEntity ResolveKeyed<TEntity>(this IHasIoC self, object serviceKey)
            where TEntity : notnull
        {
            return self.IoC.ResolveKeyed<TEntity>(serviceKey);
        }
    }
}