﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.IO;
using Zerox.Core.App;
using Zerox.Core.Autofac;
using Zerox.Core.Config;

namespace Zerox.Core.XUnit
{

    /// <summary>
    /// This is base xUnit Fixture, it will be share among all tests
    /// we will handle IoC container and all other eventual management here
    /// </summary>
    public abstract class XUnitTestingContextBase : IDisposable, IHasIoC
    {
        public ILifetimeScope IoC { get; init; }

        public IConfiguration Config { get; init; }

        public IServiceProvider ServiceProvider { get; init; }

        protected XUnitTestingContextBase()
        {
            var tmpCfg = new ConfigurationBuilder().SetBasePath(AppExtensions.GetAppWorkingDir());
            
            
            tmpCfg.ConfigureWithAppsettings(true);
            

            var Config = tmpCfg.AddEnvironmentVariables().Build();


            // build logger first
            Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(Config)
            .CreateLogger();

            // The Microsoft.Extensions.DependencyInjection.ServiceCollection
            // has extension methods provided by other .NET Core libraries to
            // register services with DI.
            var serviceCollection = new ServiceCollection();

            // Add logging
            // to add logging services.
            serviceCollection.AddLogging(logBuilder =>
            {
                logBuilder.AddSerilog(dispose: true);
            });

            var containerBuilder = new ContainerBuilder();
            // Once you've registered everything in the ServiceCollection, call
            // Populate to bring those registrations into Autofac. This is
            // just like a foreach over the list of things in the collection
            // to add them to Autofac.
            containerBuilder.Populate(serviceCollection);

            // register config
            var r = containerBuilder.RegisterInstance(Config).As<IConfiguration>();

            // Make your Autofac registrations. Order is important!
            // If you make them BEFORE you call Populate, then the
            // registrations in the ServiceCollection will override Autofac
            // registrations; if you make them AFTER Populate, the Autofac
            // registrations will override. You can make registrations
            // before or after Populate, however you choose.
            IoC = containerBuilder.RegisterConfigFile(Config)
                            .RegisterModules(GetType().TryToGetAutofacPrefixes())
                            .RegisterCustoms(RegisterMocks)
                            // Creating a new AutofacServiceProvider makes the container
                            // available to your app using the Microsoft IServiceProvider
                            // interface so you can use those abstractions rather than
                            // binding directly to Autofac.
                            .Build();

            ServiceProvider = new AutofacServiceProvider(IoC);

            // make globbal IoC access, not nice, very antipattern, but we need it!!!
            //DIContainer.SetContainer(IoC);
        }

        /// <summary>
        /// Overriden fixtures can use this for override registrations
        /// </summary>
        /// <param name="builder"></param>
        protected virtual void RegisterMocks(ContainerBuilder builder)
        {
            // can be overriden
        }

        

        #region DISPOSE

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    IoC.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~XUnitTestingContextBase()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion DISPOSE
    }
}