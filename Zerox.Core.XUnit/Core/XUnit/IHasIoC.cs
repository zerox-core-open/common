﻿using Autofac;

namespace Zerox.Core.XUnit
{
    public interface IHasIoC
    {
        ILifetimeScope IoC { get; }
    }
}