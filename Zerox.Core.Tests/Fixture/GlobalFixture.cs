﻿using Autofac;
using Xunit;
using Zerox.Core.Autofac;
using Zerox.Core.XUnit;

namespace Zerox.Core.Tests.Fixture
{
    // this should register also from zerox.* assemblies
    [AutofacModulePrefixes("Zerox")]
    public class GlobalFixture : XUnitTestingContextBase
    {
        protected override void RegisterMocks(ContainerBuilder builder)
        {
        }
    }

    [CollectionDefinition("Zerox.Core.Tests")]
    public class DatabaseCollection : ICollectionFixture<GlobalFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}