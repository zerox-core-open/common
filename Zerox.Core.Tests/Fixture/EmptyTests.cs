﻿using Autofac;
using Microsoft.Extensions.Logging;
using Xunit;

namespace Zerox.Core.Tests.Fixture
{
    [Collection("Zerox.Core.Tests")]
    public class EmptyTests
    {
        #region INIT

        private GlobalFixture _fixture;
        private ILogger Logger { get; init; }

        // this will inject fixture
        public EmptyTests(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<EmptyTests>>();
        }

        #endregion INIT

        [Fact]
        public void EmptyTest()
        {
            Assert.True(true);
        }

        [Theory]
        [InlineData(0)]
        public void EmptyTheory(int a)
        {
            Assert.True(a == 0);
        }
    }
}