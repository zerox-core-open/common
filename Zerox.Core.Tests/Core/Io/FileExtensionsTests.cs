﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Zerox.Core.Io;
using Zerox.Core.XUnit;
using Zerox.Core.Support;
using System.Linq;
using Zerox.Core.Tests.Fixture;
using Microsoft.Extensions.Logging;
using Autofac;
using System.Security.Cryptography;

namespace Zerox.Core.Io
{
    public enum SomeFileKind : int
    {
        undef = 0
    }

    [Collection("Zerox.Core.Tests")]
    public class FileExtensionsTests
    {
        #region INIT

        private GlobalFixture _fixture;
        private ILogger Logger { get; init; }

        // this will inject fixture
        public FileExtensionsTests(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<FileExtensionsTests>>();
        }

        #endregion INIT

        [Fact]
        public void ExtractInfoTest()
        {
            var infoF = new FileInfo(Path.Combine("Assets", "Medium", "recipe.json"));
            var extInfo = infoF.ExtractInfo();

            Assert.Equal(File.OpenRead(Path.Combine("Assets", "Medium", "recipe.json").ToString()).ToMD5().ToString().ToUpper(), extInfo.Md5.ToUpper());
            Assert.Equal(infoF.LastWriteTime.Ticks, extInfo.LastWrite.Ticks); // 23.03.2022 08:41:53...
        }

        [Theory]
        [InlineData(FileExtensions.CompressionMethodEnum.Deflate)]
        [InlineData(FileExtensions.CompressionMethodEnum.Lz4)]
        public async Task CompresionInStreamTest(FileExtensions.CompressionMethodEnum method)
        {
            var origFilePath = Path.Combine("Assets", "Medium", "recipe.json");
            var tmpFile = @"c:\tmp\compr" + (method == FileExtensions.CompressionMethodEnum.Lz4 ? ".lz4" : ".dfl");
            var tmpJsonFile = @"c:\tmp\compr_check.json";
            var origInfo = new FileInfo(origFilePath);
            
            using(var comp = await origInfo.CompressInMemoryStreamAsync<CompressionExtendedInfo<SomeFileKind>, SomeFileKind>(SomeFileKind.undef, method, "MachineOne/Recipe.RecipeNote"))
            {
                // dump it in the file
                using(FileStream outS = File.Create(tmpFile)){
                    await comp.Stream.CopyToAsync(outS);
                }

                
                // compressed file
                var compFinfo = new FileInfo(tmpFile);
                var extInfo = compFinfo.ExtractInfo();
                Assert.Equal(extInfo.Md5, comp.Info.CompressedHash);
                Assert.Equal(extInfo.Mime, comp.Info.CompressedMime);
                Assert.Equal(extInfo.Size, comp.Info.CompressedSize);
                Assert.Equal(extInfo.Ext, comp.Info.CompressedExt);
                // original referenced properties
                var origExtInfo = origInfo.ExtractInfo();
                Assert.Equal(origExtInfo.Md5, comp.Info.Hash);
                Assert.Equal(origExtInfo.NameWithoutExt, comp.Info.NameWithoutExt);
                Assert.Equal(origInfo.FullName, comp.Info.FileFullName);
                Assert.Equal(origExtInfo.Ext, comp.Info.FileExtension);
                Assert.Equal(origExtInfo.LastWrite, comp.Info.LastWrite);
                Assert.Equal(origExtInfo.Size, comp.Info.Size);
                Assert.Equal("Note 1", comp.Info.Description);
            }

            // decompress it back
            using (FileStream inStr = File.OpenRead(tmpFile))
            using (var uncompStrm = inStr.GetDecompressStream(method))
            using (var outFile = File.Create(tmpJsonFile))
            {
                await uncompStrm.CopyToAsync(outFile);
            }

            // check bynary content
            var byteArrayOrg = File.ReadAllBytes(origFilePath);
            var byteArrayCp = File.ReadAllBytes(tmpJsonFile);
            Assert.True(Enumerable.SequenceEqual(byteArrayCp, byteArrayOrg));

        }

        [Theory]
        [InlineData(".gz", "recipe.json")]
        [InlineData(".lz4", "recipe.json")]
        [InlineData(".dfl", "recipe.json")]
        [InlineData(".gz", "some.png")]
        [InlineData(".lz4", "some.png")]
        [InlineData(".dfl", "some.png")]
        public async Task StreamPipelineTest(string ext, string what)
        {
            for (int i = 0; i < 100; ++i)
            {
                //read from tests settings
                // var configuration = _fixture.Config;

                var outPath = @"C:\tmp\" + what + ext;
                var filePath = @"Assets\Medium\" + what;
                var recipeJsonTmp = @"C:\tmp\" + what;

                var hasher = MD5.Create();
                var oHasher = MD5.Create();

                var infoFOrig = new FileInfo(filePath);

                using (FileStream outFile = File.Create(outPath))
                {
                    using (CryptoStream crypto = new CryptoStream(outFile, hasher, CryptoStreamMode.Write, true))
                    using (var compress = crypto.GetCompressStream(outPath.ToCompressionMethod()))
                    using (CryptoStream oCrypto = new CryptoStream(compress, oHasher, CryptoStreamMode.Write))
                    using (FileStream source = File.OpenRead(filePath))
                    {
                        await source.CopyToAsync(oCrypto);
                    }


                    var length = outFile.Position; //3468
                }
                
                // at this point the streams are closed so the hash is ready
                string hash = BitConverter.ToString(hasher.Hash).Replace("-", "").ToLowerInvariant();

                var infoF = new FileInfo(outPath);
                var extInfo = infoF.ExtractInfo();
                Assert.True(hash == extInfo.Md5);
                try
                {
                    using (FileStream source = File.OpenRead(outPath))
                    using (var decompress = source.GetDecompressStream(outPath.ToCompressionMethod()))
                    using (FileStream of = File.Create(recipeJsonTmp))
                    {
                        await decompress.CopyToAsync(of);
                    }
                }
                catch (Exception ex)
                {
                    ;
                }

                var byteArrayOrg = File.ReadAllBytes(filePath);
                var byteArrayCp = File.ReadAllBytes(recipeJsonTmp);
                Assert.True(Enumerable.SequenceEqual(byteArrayCp, byteArrayOrg));
            }
        }
    }
}
