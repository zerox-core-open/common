﻿using Autofac;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Zerox.Core.Tests.Fixture;

namespace Zerox.Core.Async
{
    public class Cnts
    {
        public int OptimisticLock = 0;
        public int Good = 0;
        public int Wrong = 0;

        public void Work(ref int optlock)
        {
            optlock = OptimisticLock;

            Thread.Sleep(1); // do posibility to mix threads

            ++OptimisticLock;

            if (OptimisticLock == optlock + 1)
            {
                ++Good;
            }
            else
            {
                ++Wrong;
            }
        }
    }

    [Collection("Zerox.Core.Tests")]
    public class CryticalSectionTest
    {
        #region INIT

        private GlobalFixture _fixture;
        private ILogger Logger { get; init; }

        // this will inject fixture
        public CryticalSectionTest(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<CryticalSectionTest>>();
        }

        #endregion INIT

        protected bool WaitUntil(Func<bool> f, int timeout = 1000)
        {
            int step = 10;
            while (timeout > 0)
            {
                if (f()) return true;

                timeout -= step;
                Thread.Sleep(step);
            }
            return false;
        }

        [Fact]
        public async Task Test_CriticalSectionAsyncCompatibilityAsync()
        {
            CriticalSection cs = new CriticalSection();

            Exception eee = default;

            try
            {

                using (cs.Lock(TimeSpan.FromSeconds(100)))
                {
                    await Task.Delay(100).ConfigureAwait(false); // simulate threading context problem
                }

            }
            catch (Exception ex)
            {
                eee = ex;
            }

            Assert.True(eee == default);

        }

        [Fact]
        public async Task Test_LockAsyncProblemAsync()
        {


            Exception eee = default;

            object _lock = new object();

            TimedLock cs = new TimedLock(_lock);

            try
            {

                using (cs.Lock(TimeSpan.FromSeconds(100)))
                {
                    await Task.Delay(100).ConfigureAwait(false); // simulate threading context problem
                }

            }
            catch (Exception ex)
            {
                eee = ex;
            }

            Assert.True(eee is SynchronizationLockException);

        }


        [Fact]
        public void Test_CriticalSection()
        {
            CriticalSection cs = new CriticalSection();
            int done = 0;
            Cnts cnts = new Cnts();

            ManualResetEvent evt = new ManualResetEvent(false);


            ThreadPool.QueueUserWorkItem(new WaitCallback((o) =>
            {
                int optlock = 0;
                // wait here for parallel start
                evt.WaitOne();
                for (int i = 0; i < 100; ++i)
                {
                    using (cs.Lock(TimeSpan.FromSeconds(100)))
                    {
                        Task.Delay(13).SafeFireAndForget(false); // simulate threading context problem
                        cnts.Work(ref optlock);
                    }
                }
                ++done;
            }));

            ThreadPool.QueueUserWorkItem(new WaitCallback((o) =>
            {
                int optlock = 0;
                // wait here for parallel start
                evt.WaitOne();
                for (int i = 0; i < 100; ++i)
                {
                    using (cs.Lock(TimeSpan.FromSeconds(100)))
                    {
                        cnts.Work(ref optlock);
                        Task.Delay(10).SafeFireAndForget(false); // simulate threading context problem
                    }
                }
                ++done;
            }));


            ThreadPool.QueueUserWorkItem(new WaitCallback((o) =>
            {
                int optlock = 0;
                // wait here for parallel start
                evt.WaitOne();
                for (int i = 0; i < 100; ++i)
                {
                    using (cs.Lock(TimeSpan.FromSeconds(100)))
                    {
                        cnts.Work(ref optlock);
                    }
                }
                ++done;
            }));

            // run all
            evt.Set();

            // wait until done
            Assert.True(WaitUntil(() => done == 3, 10000));

            Assert.True(cnts.Wrong == 0);
            Assert.True(cnts.Good == 300);

        }

        [Fact]
        public void Test_No_CriticalSection()
        {
            int done = 0;
            Cnts cnts = new Cnts();

            ManualResetEvent evt = new ManualResetEvent(false);


            ThreadPool.QueueUserWorkItem(new WaitCallback((o) =>
            {
                int optlock = 0;
                // wait here for parallel start
                evt.WaitOne();
                for (int i = 0; i < 100; ++i)
                {
                    cnts.Work(ref optlock);
                }
                ++done;
            }));

            ThreadPool.QueueUserWorkItem(new WaitCallback((o) =>
            {
                int optlock = 0;
                // wait here for parallel start
                evt.WaitOne();
                for (int i = 0; i < 100; ++i)
                {
                    cnts.Work(ref optlock);
                }
                ++done;
            }));

            ThreadPool.QueueUserWorkItem(new WaitCallback((o) =>
            {
                int optlock = 0;
                // wait here for parallel start
                evt.WaitOne();
                for (int i = 0; i < 100; ++i)
                {
                    cnts.Work(ref optlock);
                }
                ++done;
            }));

            // run all
            evt.Set();

            // wait until done
            Assert.True(WaitUntil(() => done == 3, 10000));

            Assert.True(cnts.Wrong > 0);
            Assert.True(cnts.Good != 300);

        }
    }
}