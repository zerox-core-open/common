﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;
using Zerox.Core.App;
using Zerox.Core.Config;
using Zerox.Core.Tests.Fixture;

namespace Zerox.Core.Json
{
    
    [Collection("Zerox.Core.Tests")]
    public class IConfigurationBuilderExtensionsTest
    {
        #region INIT

        private GlobalFixture _fixture;
        private ILogger Logger { get; init; }

        // this will inject fixture
        public IConfigurationBuilderExtensionsTest(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<IConfigurationBuilderExtensionsTest>>();
        }

        #endregion INIT

        [Fact]
        public void ConfigLoad()
        {
            var baseDir = Path.Combine(AppExtensions.GetAppWorkingDir(), "cfgtest");

            if (Directory.Exists(baseDir))
            {
                Directory.Delete(baseDir, true);
            }

            Directory.CreateDirectory(baseDir);

            var cfg = "{\"Value\": 0}";             File.WriteAllText(Path.Combine(baseDir, $"appsettings.json"), cfg);
            var cfg_api = "{\"Value\": 1}";         File.WriteAllText(Path.Combine(baseDir, $"appsettings-api.json"), cfg_api);
            var cfg_api_pc = "{\"Value\": 2}";      File.WriteAllText(Path.Combine(baseDir, $"appsettings-api-{Environment.MachineName}.json"), cfg_api_pc);
            var cfg_test = "{\"Value\": 10}";       File.WriteAllText(Path.Combine(baseDir, $"appsettings-test.json"), cfg_test);
            

            { // -
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(new List<(string, bool)>() { 
                    
                }, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 0);
            }

            { // api
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(new List<(string, bool)>()
                {
                    new ("api", true)
                }, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 1);
            }

            { // api-pc
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(new List<(string, bool)>()
                {
                    new ("api", true),
                    new ($"api-{Environment.MachineName}", true)
                }, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 2);
            }

            { // api-pc
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(new List<(string, bool)>()
                {
                    new ("api", true),
                    new ($"api-{Environment.MachineName}", true),
                    new ($"pippo", false)
                }, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 2);
            }

            { // api-pc
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                Assert.Throws<ApplicationException>(() =>
                {
                    tmpCfg.ConfigureWithAppsettings(new List<(string, bool)>()
                    {
                        new ("api", true),
                        new ($"api-{Environment.MachineName}", true),
                        new ($"pippo", true)
                    }, baseDir);
                });
            }

            { // api auto
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(false, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 2);
            }

            { // test auto
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(true, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 10);
            }

            var cfg_test_pc = "{\"Value\": 20}"; File.WriteAllText(Path.Combine(baseDir, $"appsettings-test-{Environment.MachineName}.json"), cfg_test_pc);

            { // test auto
                var tmpCfg = new ConfigurationBuilder().SetBasePath(baseDir);

                tmpCfg.ConfigureWithAppsettings(true, baseDir);

                var config = tmpCfg.Build();
                Assert.True(config.GetValue<int>("Value") == 20);
            }
        }
    }
}