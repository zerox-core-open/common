﻿using Autofac;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xunit;
using Zerox.Core.Tests.Fixture;
using Zerox.Dynamic;

namespace Zerox.Dynamic
{
    public interface IMyInterface
    {
    }
}

namespace Zerox.Core.Data.Dynamic
{
    [Collection("Zerox.Core.Tests")]
    public class DynamicClassTests
    {
        #region INIT

        private GlobalFixture _fixture;
        private ILogger Logger { get; init; }

        // this will inject fixture
        public DynamicClassTests(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<DynamicClassTests>>();
        }

        #endregion INIT

        internal class Examples
        {
            [Range(10, 10, ConvertValueInInvariantCulture = false, ErrorMessage = "Some error")]
            [Required]
            public int A { get; set; }
        }

        [Fact]
        public void AttributeStringDumpTest()
        {
            {
                var da = new DynamicAttrib(typeof(RangeAttribute), new List<(string, object)>() {
                    (default, 10), (default, 20), (nameof(RangeAttribute.ConvertValueInInvariantCulture), false), (nameof(RangeAttribute.ErrorMessage), "Some error")
                });
                Assert.True(da.ToString() == "[Range(10, 20, ConvertValueInInvariantCulture = false, ErrorMessage = \"Some error\")]");
            }

            {
                var da = new DynamicAttrib(typeof(RequiredAttribute));
                Assert.True(da.ToString() == "[Required]");
            }
        }

        [Fact]
        public void ClassTest()
        {
            var check = @"using System;
using System.ComponentModel.DataAnnotations;

namespace Zerox.Dynamic {
	[Serializable]
	public class MyClass : IMyInterface
	{
		[Required]
		[Range(10, 20)]
		public int Age { get; set; }

		[Required]
		public string Name { get; set; }

		public string? Tag { get; set; }
		
	}
}";
            var dt = new DynamicClass("MyClass", "Zerox.Dynamic")
                .Implement<IMyInterface>()
                .WithAttribute(DynamicAttrib.From<SerializableAttribute>())
                .WithProperty<int>("Age", (prop) =>
                    prop.WithAttribute(DynamicAttrib.From<RequiredAttribute>())
                        .WithAttribute(DynamicAttrib.From<RangeAttribute>(new List<(string, object)>() { (default, 10), (default, 20) }))

                 )
                .WithProperty<string>("Name", (prop) => prop.WithAttribute(DynamicAttrib.From<RequiredAttribute>()))
                .WithProperty<string>("Tag");

            string cls = dt.ToString();

            Assert.True(cls == check);
        }
    }
}