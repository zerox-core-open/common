﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Zerox.Core.Data.Dynamic
{
    public class ValueHelperTest
    {
        private static readonly string EOL = Environment.NewLine;

        [Theory]
        [InlineData(
            "single-line string with \"",
            "\"single-line string with \\\"\"")]
        [InlineData(
            true,
            "true")]
        [InlineData(
            false,
            "false")]
        [InlineData(
            (byte)42,
            "(byte)42")]
        [InlineData(
            'A',
            "'A'")]
        [InlineData(
            '\'',
            @"'\''")]
        [InlineData(
            4.2,
            "4.2000000000000002")]
        [InlineData(
            double.NegativeInfinity,
            "double.NegativeInfinity")]
        [InlineData(
            double.PositiveInfinity,
            "double.PositiveInfinity")]
        [InlineData(
            double.NaN,
            "double.NaN")]
        [InlineData(
            0.84551240822557006,
            "0.84551240822557006")]
        [InlineData(
            6E-14,
            "5.9999999999999997E-14")]
        [InlineData(
            -1.7976931348623157E+308, // Double MinValue
            "-1.7976931348623157E+308")]
        [InlineData(
            1.7976931348623157E+308, // Double MaxValue
            "1.7976931348623157E+308")]
        [InlineData(
            4.2f,
            "4.2f")]
        [InlineData(
            -3.402823E+38f, // Single MinValue
            "-3.402823E+38f")]
        [InlineData(
            3.402823E+38f, // Single MaxValue
            "3.402823E+38f")]
        [InlineData(
            42,
            "42")]
        [InlineData(
            42L,
            "42L")]
        [InlineData(
            9000000000000000000L, // Ensure not printed as exponent
            "9000000000000000000L")]
        [InlineData(
            (sbyte)42,
            "(sbyte)42")]
        [InlineData(
            (short)42,
            "(short)42")]
        [InlineData(
            42u,
            "42u")]
        [InlineData(
            42ul,
            "42ul")]
        [InlineData(
            18000000000000000000ul, // Ensure not printed as exponent
            "18000000000000000000ul")]
        [InlineData(
            (ushort)42,
            "(ushort)42")]
        [InlineData(
            "",
            "\"\"")]
        [InlineData(
            SomeEnum.Default,
            "ValueHelperTest.SomeEnum.Default")]
        public void Literal_works(object value, string expected)
        {
            var literal = DynamicClass.Helper.UnknownLiteral(value);
            Assert.Equal(expected, literal);
        }

        [Fact]
        public void Literal_works_when_empty_ByteArray()
            => Literal_works(
                Array.Empty<byte>(),
                "new byte[0]");

        [Fact]
        public void Literal_works_when_single_ByteArray()
            => Literal_works(
                new byte[] { 1 },
                "new byte[] { 1 }");

        [Fact]
        public void Literal_works_when_many_ByteArray()
            => Literal_works(
                new byte[] { 1, 2 },
                "new byte[] { 1, 2 }");

        [Fact]
        public void Literal_works_when_multiline_string()
            => Literal_works(
                "multi-line\r\nstring\nwith\r\"",
                "\"multi-line\\r\\nstring\\nwith\\r\\\"\"");

        [Fact]
        public void Literal_works_when_DateTime()
            => Literal_works(
                new DateTime(2015, 3, 15, 20, 45, 17, 300, DateTimeKind.Local),
                "new DateTime(2015, 3, 15, 20, 45, 17, 300, DateTimeKind.Local)");

        [Fact]
        public void Literal_works_when_DateTimeOffset()
            => Literal_works(
                new DateTimeOffset(new DateTime(2015, 3, 15, 19, 43, 47, 500), new TimeSpan(-7, 0, 0)),
                "new DateTimeOffset(new DateTime(2015, 3, 15, 19, 43, 47, 500, DateTimeKind.Unspecified), new TimeSpan(0, -7, 0, 0, 0))");

        [Fact]
        public void Literal_works_when_decimal()
            => Literal_works(
                4.2m,
                "4.2m");

        [Fact]
        public void Literal_works_when_decimal_max_value()
            => Literal_works(
                79228162514264337593543950335m, // Decimal MaxValue
                "79228162514264337593543950335m");

        [Fact]
        public void Literal_works_when_decimal_min_value()
            => Literal_works(
                -79228162514264337593543950335m, // Decimal MinValue
                "-79228162514264337593543950335m");

        [Fact]
        public void Literal_works_when_Guid()
            => Literal_works(
                new Guid("fad4f3c3-9501-4b3a-af99-afeb496f7664"),
                "new Guid(\"fad4f3c3-9501-4b3a-af99-afeb496f7664\")");

        [Fact]
        public void Literal_works_when_TimeSpan()
            => Literal_works(
                new TimeSpan(17, 21, 42, 37, 250),
                "new TimeSpan(17, 21, 42, 37, 250)");

        [Fact]
        public void Literal_works_when_NullableInt()
            => Literal_works(
                (int?)42,
                "42");

        [Fact]
        public void Literal_works_when_StringArray()
        {
            var literal = DynamicClass.Helper.Literal(new[] { "A", "B" });
            Assert.Equal("new[] { \"A\", \"B\" }", literal);
        }

        [Fact]
        public void Literal_works_when_empty_StringArray()
        {
            var literal = DynamicClass.Helper.Literal(new string[] { });
            Assert.Equal("new string[0]", literal);
        }

        [Fact]
        public void Literal_works_when_ObjectArray()
        {
            var literal = DynamicClass.Helper.Literal(new object[] { 'A', 1 });
            Assert.Equal("new object[] { 'A', 1 }", literal);
        }

        [Fact]
        public void Literal_works_when_MultidimensionalArray()
        {
            var value = new object[,] { { 'A', 1 }, { 'B', 2 } };

            var result = DynamicClass.Helper.Literal(value);

            Assert.Equal(
                "new object[,]" + EOL + "{" + EOL + "    { 'A', 1 }," + EOL + "    { 'B', 2 }" + EOL + "}",
                result);
        }

        [Fact]
        public void Literal_works_when_BigInteger()
            => Literal_works(
                new BigInteger(42),
                "BigInteger.Parse(\"42\", NumberFormatInfo.InvariantInfo)");

        [Fact]
        public void UnknownLiteral_throws_when_unknown()
        {
            var ex = Assert.Throws<InvalidOperationException>(
                () => DynamicClass.Helper.UnknownLiteral(new object()));
            Assert.Equal($"Unknown type: ({typeof(object).Name})", ex.Message);
        }

        [Theory]
        [InlineData(typeof(int), "int")]
        [InlineData(typeof(int?), "int?")]
        [InlineData(typeof(int[]), "int[]")]
        [InlineData(typeof(int[,]), "int[,]")]
        [InlineData(typeof(int[][]), "int[][]")]
        [InlineData(typeof(Generic<int>), "Generic<int>")]
        [InlineData(typeof(Nested), "ValueHelperTest.Nested")]
        [InlineData(typeof(Generic<Generic<int>>), "Generic<Generic<int>>")]
        [InlineData(typeof(MultiGeneric<int, int>), "MultiGeneric<int, int>")]
        [InlineData(typeof(NestedGeneric<int>), "ValueHelperTest.NestedGeneric<int>")]
        [InlineData(typeof(Nested.DoubleNested), "ValueHelperTest.Nested.DoubleNested")]
        public void Reference_works(Type type, string expected)
            => Assert.Equal(expected, DynamicClass.Helper.Reference(type));

        private static class Nested
        {
            public class DoubleNested
            {
            }
        }

        internal class NestedGeneric<T>
        {
        }

        private enum SomeEnum
        {
            Default
        }

        [Theory]
        [InlineData("dash-er", "dasher")]
        [InlineData("params", "@params")]
        [InlineData("true", "@true")]
        [InlineData("yield", "yield")]
        [InlineData("spac ed", "spaced")]
        [InlineData("1nders", "_1nders")]
        [InlineData("name.space", "@namespace")]
        [InlineData("$", "_")]
        public void Identifier_works(string input, string expected)
        {
            Assert.Equal(expected, DynamicClass.Helper.Identifier(input));
        }

        [Theory]
        [InlineData(new[] { "WebApplication1", "Migration" }, "WebApplication1.Migration")]
        [InlineData(new[] { "WebApplication1.Migration" }, "WebApplication1.Migration")]
        [InlineData(new[] { "ef-xplat.namespace" }, "efxplat.@namespace")]
        [InlineData(new[] { "#", "$" }, "_._")]
        [InlineData(new[] { "" }, "_")]
        [InlineData(new string[] { }, "_")]
        [InlineData(new string[] { null }, "_")]
        public void Namespace_works(string[] input, string excepted)
        {
            Assert.Equal(excepted, DynamicClass.Helper.Namespace(input));
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works()
        {
            var method = new MethodCallCodeFragment("Test", true, 42);

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".Test(true, 42)", result);
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works_with_arrays()
        {
            var method = new MethodCallCodeFragment("Test", new byte[] { 1, 2 }, new[] { 3, 4 }, new[] { "foo", "bar" });

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".Test(new byte[] { 1, 2 }, new[] { 3, 4 }, new[] { \"foo\", \"bar\" })", result);
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works_when_niladic()
        {
            var method = new MethodCallCodeFragment("Test");

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".Test()", result);
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works_when_chaining()
        {
            var method = new MethodCallCodeFragment("Test")
                .Chain("Test");

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".Test().Test()", result);
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works_when_chaining_on_chain()
        {
            var method = new MethodCallCodeFragment("One", Array.Empty<object>(), new MethodCallCodeFragment("Two"))
                .Chain("Three");

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".One().Two().Three()", result);
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works_when_chaining_on_chain_with_call()
        {
            var method = new MethodCallCodeFragment("One", Array.Empty<object>(), new MethodCallCodeFragment("Two"))
                .Chain(new MethodCallCodeFragment("Three", Array.Empty<object>(), new MethodCallCodeFragment("Four")));

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".One().Two().Three().Four()", result);
        }

        [Fact]
        public void Fragment_MethodCallCodeFragment_works_when_nested_closure()
        {
            var method = new MethodCallCodeFragment(
                "Test",
                new NestedClosureCodeFragment("x", new MethodCallCodeFragment("Test")));

            var result = DynamicClass.Helper.Fragment(method);

            Assert.Equal(".Test(x => x.Test())", result);
        }    
        
    }

    internal class SimpleTestType
    {
        public static readonly int SomeStaticField = 8;
        public readonly int SomeField = 8;
        public static int SomeStaticProperty { get; } = 8;
        public int SomeInstanceProperty { get; } = 8;

        public SimpleTestType()
        {
        }

        public SimpleTestType(string arg1)
            : this(arg1, null)
        {
        }

        public SimpleTestType(string arg1, int? arg2)
        {
            Arg1 = arg1;
            Arg2 = arg2;
        }

        public string Arg1 { get; }
        public int? Arg2 { get; }
    }

    internal class SimpleTestTypeFactory
    {
        public SimpleTestTypeFactory()
        {
        }

        public SimpleTestTypeFactory(string factoryArg)
        {
            FactoryArg = factoryArg;
        }

        public string FactoryArg { get; }

        public SimpleTestType Create()
            => new SimpleTestType();

        public object Create(string arg1)
            => new SimpleTestType(arg1);

        public object Create(string arg1, int? arg2)
            => new SimpleTestType(arg1, arg2);

        public static SimpleTestType StaticCreate()
            => new SimpleTestType();

        public static object StaticCreate(string arg1)
            => new SimpleTestType(arg1);

        public static object StaticCreate(string arg1, int? arg2)
            => new SimpleTestType(arg1, arg2);
    }

    internal class Generic<T>
    {
    }

    internal class MultiGeneric<T1, T2>
    {
    }
}
