﻿using Autofac;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Newtonsoft.Json;
using Xunit;
using Zerox.Core.Tests.Fixture;

namespace Zerox.Core.Json
{
    public interface ISomeInterface
    {
        int i { get; set; }
    }

    public class SomeClass : ISomeInterface
    {
        public int i { get; set; }
    }

    public interface ISomeData
    {
        ISomeInterface someInterface { get; }
        string Name { get; }
        ObjectId Id { get; }
    }

    public record SomeRecord(
        [JsonConverter(typeof(ConcreteConverter<SomeClass>))]
        ISomeInterface someInterface,
        string Name,
        [JsonConverter(typeof(ObjectIdConverter))]
        ObjectId Id
    ) : ISomeData;

    [Collection("Zerox.Core.Tests")]
    public class ConcreteConverterTest
    {
        #region INIT

        private GlobalFixture _fixture;
        private ILogger Logger { get; init; }

        // this will inject fixture
        public ConcreteConverterTest(GlobalFixture f)
        {
            _fixture = f;
            Logger = _fixture.IoC.Resolve<ILogger<ConcreteConverterTest>>();
        }

        #endregion INIT

        [Fact]
        public void DeserializeTest()
        {
            var oid = ObjectId.GenerateNewId();

            var sr = new SomeRecord(
                new SomeClass()
                {
                    i = 10
                },
                "pippo",
                oid
            );

            var json = JsonConvert.SerializeObject(sr);
            Assert.Equal(json, $"{{\"someInterface\":{{\"i\":10}},\"Name\":\"pippo\",\"Id\":\"{oid}\"}}");

            var sr1 = JsonConvert.DeserializeObject<SomeRecord>(json);
            Assert.Equal(sr.Name, sr1.Name);
            Assert.Equal(sr.someInterface.i, sr1.someInterface.i);
            Assert.Equal(sr.Id, sr1.Id);
        }
    }
}