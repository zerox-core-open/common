﻿using System;
using System.IO;

namespace Zerox.Core.App
{
    public static class AppExtensions
    {
        public static string GetAppWorkingDir()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public static string GetAppFilePath(string subDir, string fileName)
        {
            return GetAppDirPath(subDir) + Path.DirectorySeparatorChar + fileName;
        }

        public static string GetAppDirPath(string subDir)
        {
            try
            {
                string path = GetAppWorkingDir() + Path.DirectorySeparatorChar + subDir;
                Directory.CreateDirectory(path);
                return path;
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Can't make path for: {subDir}", ex);
            }
        }
    }
}