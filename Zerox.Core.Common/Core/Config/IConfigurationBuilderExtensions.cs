﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Zerox.Core.App;

namespace Zerox.Core.Config
{
    public static class IConfigurationBuilderExtensions
    {
        public static void ConfigureWithAppsettings(this IConfigurationBuilder config, bool isTest = false, string? workDir = default)
        {
            List<(string, bool)> overloads = new List<(string, bool)>()
            {
                new (isTest ? "test" : "api", true),
                new ($"{(isTest ? "test" : "api")}-{Environment.MachineName}", false)
            };

            ConfigureWithAppsettings(config, overloads, workDir);
        }

        public static void ConfigureWithAppsettings(this IConfigurationBuilder config, IEnumerable<(string, bool)> overloads, string? workDir = default)
        {
            var baseDir = workDir != default ? workDir : AppExtensions.GetAppWorkingDir();

            config.SetBasePath(baseDir);

            if (File.Exists(Path.Combine(AppExtensions.GetAppWorkingDir(), "appsettings.json")))
            {
                config.AddJsonFile("appsettings.json");
            }
            else
            {
                throw new ApplicationException("appsettings.json!");
            }

            foreach(var o in overloads)
            {
                if (File.Exists(Path.Combine(baseDir, $"appsettings-{o.Item1}.json")))
                {
                    config.AddJsonFile($"appsettings-{o.Item1}.json");
                }
                else
                {
                    if (o.Item2) // required?
                    {
                        throw new ApplicationException($"Missing appsettings-{o.Item1}.json and is required, exiting!");
                    }
                }
            }
        }

        public static IEnumerable<string> GetConfigurationFiles(bool isTest = false, string? workDir = default)
        {
            List<(string, bool)> overloads = new List<(string, bool)>()
            {
                new (isTest ? "test" : "api", true),
                new ($"{(isTest ? "test" : "api")}-{Environment.MachineName}", false)
            };

            var baseDir = workDir != default ? workDir : AppExtensions.GetAppWorkingDir();

            if (File.Exists(Path.Combine(AppExtensions.GetAppWorkingDir(), "appsettings.json")))
            {
                yield return "appsettings.json";
            }
            else
            {
                throw new ApplicationException("appsettings.json!");
            }

            foreach (var o in overloads)
            {
                if (File.Exists(Path.Combine(baseDir, $"appsettings-{o.Item1}.json")))
                {
                    yield return $"appsettings-{o.Item1}.json";
                }
                else
                {
                    if (o.Item2) // required?
                    {
                        throw new ApplicationException($"Missing appsettings-{o.Item1}.json and is required, exiting!");
                    }
                }
            }
        }
    }
}
