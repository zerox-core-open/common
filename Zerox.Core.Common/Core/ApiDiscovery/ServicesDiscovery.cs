﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Zerox.Core.ApiDiscovery
{
    public class ServicesDiscovery
    {
        [JsonProperty("Services")]
        public Dictionary<string, string> Services { get; protected set; }

        public ServicesDiscovery(Dictionary<string, string> services)
        {
            Services = services;
        }
    }
}
