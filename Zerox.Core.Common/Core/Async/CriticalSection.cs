﻿using System;
using System.Threading;

namespace Zerox.Core.Async
{

    public class CriticalSection
    {
        private readonly SemaphoreSlim toLock;

        public CriticalSection()
        {
            toLock = new SemaphoreSlim(1, 1);
        }

        public ICriticalSectionLock Lock()
        {
            toLock.Wait();
            return new LockReleaser(toLock);
        }

        public ICriticalSectionLock Lock(TimeSpan timeout)
        {
            if (toLock.Wait(timeout))
            {
                return new LockReleaser(toLock);
            }
            throw new TimeoutException();
        }

        public ICriticalSectionLock TryLock()
        {
            if (toLock.Wait(0))
            {
                return new LockReleaser(toLock);
            }
            else
            {
                return new DummyLockReleaser();
            }
        }

        public ICriticalSectionLock TryLock(TimeSpan timeout)
        {
            if (toLock.Wait(timeout))
            {
                return new LockReleaser(toLock);
            }
            else
            {
                return new DummyLockReleaser();
            }
        }

        public struct LockReleaser : ICriticalSectionLock
        {
            private readonly SemaphoreSlim toRelease;

            public LockReleaser(SemaphoreSlim toRelease)
            {
                this.toRelease = toRelease;
            }

            public bool IsLocked => true;

            public void Dispose()
            {
                toRelease.Release();
            }
        }

        public struct DummyLockReleaser : ICriticalSectionLock
        {
            public bool IsLocked => false;

            public void Dispose()
            {
            }
        }
    }
}