﻿using System;

namespace Zerox.Core.Async
{
    public interface ICriticalSectionLock : IDisposable
    {
        bool IsLocked { get; }
    }
}