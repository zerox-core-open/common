﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Zerox.Core.Async
{
    /// <summary>
    /// Helper class to run async methods within a sync process.
    /// </summary>
    public static class AsyncUtil
    {

        /// <summary>
        /// Executes an async Task method which has a void return value synchronously
        /// USAGE: AsyncUtil.RunSync(() => AsyncMethod());
        /// </summary>
        /// <param name="task">Task method to execute</param>
        public static void RunSync(Func<Task> task)
        {
            var capturedContext = SynchronizationContext.Current;
            try
            {
                // Wipe the sync context, so that the bad library code won't find it
                // That way, we avoid the deadlock
                SynchronizationContext.SetSynchronizationContext(null);

                // Call the async method and wait for the result
                task().Wait();
            }
            finally
            {
                // Restore the sync context
                SynchronizationContext.SetSynchronizationContext(capturedContext);
            }
        }

        /// <summary>
        /// Executes an async Task<T> method which has a T return type synchronously
        /// USAGE: T result = AsyncUtil.RunSync(() => AsyncMethod<T>());
        /// </summary>
        /// <typeparam name="TResult">Return Type</typeparam>
        /// <param name="task">Task<T> method to execute</param>
        /// <returns></returns>
        public static TResult RunSync<TResult>(Func<Task<TResult>> task)
        {
            var capturedContext = SynchronizationContext.Current;
            try
            {
                // Wipe the sync context, so that the bad library code won't find it
                // That way, we avoid the deadlock
                SynchronizationContext.SetSynchronizationContext(null);

                // Call the async method and wait for the result
                return task().GetAwaiter().GetResult();
            }
            finally
            {
                // Restore the sync context
                SynchronizationContext.SetSynchronizationContext(capturedContext);
            }
        }
    }
}