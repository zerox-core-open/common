﻿using MimeMapping;
using System.Collections.Concurrent;

namespace Zerox.Core.Support
{

    public class MetaExtractorContext
    {
        private static DummyMetaExtractor _dummy = new DummyMetaExtractor();
        private static ConcurrentDictionary<string, IMetaExtractor> _extractors = new ConcurrentDictionary<string, IMetaExtractor>();

        public static IMetaExtractor GetExtractor(string type)
        {
            if (_extractors.TryGetValue(type, out IMetaExtractor? value))
            {
                return value;
            }

            switch (type)
            {
                case KnownMimeTypes.Json:
                    var e = new JsonMetaExtractor();
                    _extractors.TryAdd(type, e);
                    return e;

                default:
                    return _dummy;
            }
        }
    }

}

