﻿namespace Zerox.Core.Support
{

    public class DummyMetaExtractor : IMetaExtractor
    {
        public string? GetMetaByPath(string file, string metaPath)
        {
            return "DummyMeta";
        }
    }

}

