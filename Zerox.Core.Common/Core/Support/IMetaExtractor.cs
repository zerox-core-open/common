﻿namespace Zerox.Core.Support
{

    public interface IMetaExtractor
    {
        string? GetMetaByPath(string file, string metaPath);
    }

}

