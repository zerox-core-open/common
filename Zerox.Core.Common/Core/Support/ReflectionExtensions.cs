﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Zerox.Core.Support
{
    public static class ReflectionExtensions
    {
        

        /// <summary>
        /// Return custom attribute of type
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static TAttribute? GetAttribute<TAttribute>(
            this ICustomAttributeProvider provider)
            where TAttribute : Attribute
        {
            var att = provider.GetCustomAttributes(
                    typeof(TAttribute), true
                ).FirstOrDefault() as TAttribute;

            if (att is TAttribute ta) return ta;

            return default;
        }


        /// <summary>
        /// Get value of property of custom attribute
        /// f.e.:
        /// string name = typeof(MyClass).GetAttributeValue((DomainNameAttribute dna) => dna.Name);
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="provider"></param>
        /// <param name="valueSelector"></param>
        /// <returns></returns>
        public static TValue? GetAttributeValue<TAttribute, TValue>(
            this ICustomAttributeProvider provider,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
            where TValue : class
        {
            var att = provider.GetCustomAttributes(
                    typeof(TAttribute), true
                ).FirstOrDefault() as TAttribute;

            if (att != null)
            {
                return valueSelector(att);
            }
            return default(TValue);
        }

        
        
        /// <summary>
        /// Enumarates all properties with particular attribute
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="ent"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static IEnumerable<(PropertyInfo, TAttribute)> GetAttributes<TAttribute>(
            this IReflect type
        )
            where TAttribute : Attribute
        {
            foreach (var pi in type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public))
            {
                var ca = pi.GetCustomAttributes(
                    typeof(TAttribute), true
                ).FirstOrDefault() as TAttribute;

                if (ca != default)
                {
                    yield return (pi, ca);
                }
            }
        }

        /// <summary>
        /// True if attribute exists
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static bool HasAttribute<TAttribute>(
            this ICustomAttributeProvider provider)
            where TAttribute : Attribute
        {
            return provider.GetCustomAttributes(
                    typeof(TAttribute), true
                ).FirstOrDefault() != default;
        }

        public static TAttribute? GetPropertyAttribute<TAttribute>(
            this IReflect type,
            string propertyName)
            where TAttribute : Attribute
        {
            return type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
                .Where(p => p.Name == propertyName)
                .FirstOrDefault()?
                .GetCustomAttributes(
                    typeof(TAttribute), true
                ).FirstOrDefault() as TAttribute;
        }

        /// <summary>
        /// Get attribute of property type, if it is Object type
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="type"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static TAttribute? GetAttributeOfPropertyType<TAttribute>(
            this IReflect type,
            string propertyName)
            where TAttribute : Attribute
        {
            return type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public)
                .Where(p => p.Name == propertyName)
                .FirstOrDefault()?
                .PropertyType
                .GetCustomAttributes(
                    typeof(TAttribute), true
                ).FirstOrDefault() as TAttribute;
        }
    }
}