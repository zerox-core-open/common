﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Zerox.Core.Support
{
    public static class CryptoExtensions
    {
        public static string ToMD5(this Stream s)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                return string.Join(string.Empty, md5.ComputeHash(s).Select(b => b.ToString("x2")));
            }
        }

        public static string ToMD5(this string s)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                var encoding = Encoding.ASCII;
                var data = encoding.GetBytes(s);

                return string.Join(string.Empty, md5.ComputeHash(data, 0, data.Length).Select(b => b.ToString("x2")));
            }
        }

    }
}