﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerox.Core.Support
{
    /// <summary>
    /// String ID composed by separated parts
    /// [Part0][Sep][Part1][Sep][PartN]
    /// where separator can be changed
    /// the separator 
    /// </summary>
    [Serializable]
    public class HierId
    {
        public const char DEFAUL_SEPARATOR = '/';

        public char Separator { get; protected set; }

        public List<string> Parts { get; protected set; }

        protected HierId(char sep = DEFAUL_SEPARATOR) : this (new string[0], sep)
        {

        }

        protected HierId(IEnumerable<string> parts, char sep = DEFAUL_SEPARATOR)
        {
            Separator = sep;
            Parts = new List<string>();
            Parts.AddRange(parts);
        }

        public static HierId Empty()
        {
            return new HierId();
        }

        public static HierId From(HierId from)
        {
            return From(from.Parts, from.Separator);
        }

        public static HierId From(HierId from, char sep = DEFAUL_SEPARATOR)
        {
            return new HierId(from.Parts, sep);
        }

        public static HierId From(IEnumerable<string> parts, char sep = DEFAUL_SEPARATOR)
        { 
            return new HierId(parts, sep);
        }

        public static HierId From(params string[] pms)
        {
            return new HierId(pms, DEFAUL_SEPARATOR);
        }

        public static HierId Parse(string what, char sep = DEFAUL_SEPARATOR)
        { 
            return new HierId(what.Split(sep), sep);
        }

        public string GetPart(int ix)
        {
            ix = Math.Min(ix, Parts.Count - 1);
            return Parts[ix];
        }

        /// <summary>
        /// made new Hier ID  skipping n starting parts
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public HierId Splice(int n = 1)
        {
            n = Math.Min(n, Parts.Count - 1);

            if(n > 0)
            {
                return new HierId(Parts.Skip(n).ToArray(), Separator);
            }
            return Empty();
        }

        public HierId CopyWithNewPrefix(HierId prefix)
        {
            return From(prefix) + Splice(prefix.Size());
        }

        public int Size() => Parts.Count();

        /// <summary>
        /// number of parent parts which are equal
        /// </summary>
        /// <param name="r"></param>
        /// <returns>0 - no part equals, n if n parts from left are equal</returns>        
        public int GetSimilarRootSize(HierId r)
        {
            for(int i = 0, end = Math.Min(Parts.Count, r.Parts.Count); i < end; ++i)
            {
                if(Parts[i] != r.Parts[i]) return i;
            }

            return Parts.Count;
        }

        public int GetNumberOfEndingMatchingParts(HierId r)
        {
            int eThis = Size();
            int eRight = r.Size();

            int ret = 0;

            for(int i = 0, end = Math.Min(eThis, eRight); i < end; ++i)
            {
                if(GetPart(eThis - 1 - i) == r.GetPart(eRight - 1 - i))
                {
                    ++ret;
                } else
                {
                    break;
                }
            }

            return ret;
        }

        public bool MatchPartsByPatternPms(HierId r, params bool[] pat)
        {
            return MatchPartsByPattern(r, pat);
        }
        public bool MatchPartsByPattern(HierId r, IEnumerable<bool> pat)
        {
            bool ret = true;
            for (int i = 0, end = Math.Min(Math.Min(Parts.Count, r.Parts.Count), pat.Count()); i < end; ++i)
            {
                if (pat.ElementAt(i))
                {
                    ret &= Parts[i] == r.Parts[i];
                    if (!ret)
                    {
                        // shorthand escape
                        return false;
                    }
                }
            }

            return ret;
        }

        public static bool operator == (HierId r, string l)
        {
            return r.Equals(l);
        }

        public static bool operator != (HierId r, string l)
        {
            return !r.Equals(l);
        }

        public static bool operator == (HierId r, HierId l)
        {
            return r.Equals(l);
        }

        public static bool operator != (HierId r, HierId l)
        {
            return !r.Equals(l);
        }

        public static HierId operator + (HierId r, string l)
        {
            r.Parts.Add(l);
            return r;
        }

        public static HierId operator + (HierId r, HierId l)
        {
            r.Parts.AddRange(l.Parts);
            return r;
        }

        public static HierId operator - (HierId r, string l)
        {
            r.Parts.Remove(l);
            return r;
        }

        public override string ToString()
        {
            return string.Join($"{Separator}", Parts);
        }

        /// <summary>
        /// Take lvl starting parts from HieriD
        /// f.e:  HID => (aaaaa/bbbbb/cccccc).ToString(2) => aaaaa/bbbbb
        /// </summary>
        /// <param name="lvl"></param>
        /// <returns></returns>
        public string ToString(int lvl)
        {
            var cnt = lvl > 0 ? Math.Min(lvl, Parts.Count) : Parts.Count;
            return string.Join($"{Separator}", Parts.Take(cnt));
        }

        public override bool Equals(object obj)
        {
            return ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

    }
}
