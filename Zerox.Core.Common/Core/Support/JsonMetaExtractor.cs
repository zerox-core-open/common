﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Zerox.Core.Support
{

    public class JsonMetaExtractor : IMetaExtractor
    {
        /// <summary>
        /// This will extract some meta property identified by metaPath
        /// </summary>
        /// <param name="file"></param>
        /// <param name="metaPath"></param>
        /// <returns></returns>
        public string? GetMetaByPath(string file, string metaPath)
        {
            try
            {
                using (StreamReader stream = File.OpenText(file))
                using (JsonTextReader reader = new JsonTextReader(stream))
                {
                    JObject o = (JObject)JToken.ReadFrom(reader);
                    JToken? meta = o.SelectToken($"$.{metaPath}");

                    if (meta != default)
                    {
                        return meta.Value<string>();
                    }
                }
            } catch
            {
                return $"ERROR: extracting meta!";
            }

            return string.Empty;
        }
    }

}

