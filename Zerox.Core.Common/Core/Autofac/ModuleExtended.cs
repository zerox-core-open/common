﻿using Autofac;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Zerox.Core.Settings;

namespace Zerox.Core.Autofac
{
    public abstract class ModuleExtended : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // workaround against multiple module registration !!!!
            var mn = GetType().AssemblyQualifiedName;
            if (mn != default)
            {
                if (builder.Properties.ContainsKey(mn))
                {
                    return;
                }
                builder.Properties.Add(mn, null);
            }
            RegisterTypes(builder);
        }

        protected abstract void RegisterTypes(ContainerBuilder builder);

        /// <summary>
        /// This try to register module configuration type, if it exists in
        /// appsettings.json, it will use it, if not it will register config with default values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Instance of registered config</returns>
        protected T? RegisterConfig<T>(ContainerBuilder builder)
            where T : class, IConfig
        {
            Type configType = typeof(T);
            /**********************************************/
            /*  Register module configuration             */
            /**********************************************/

            T? cfgInstance = default;

            object? tmp;
            if (builder.Properties.TryGetValue(AutofacExtensionsBase.APPSETTINGS_PROPERTY_KEY, out tmp))
            {
                // bind config from appsettings.json
                if (tmp is IConfiguration cfg)
                {
                    cfgInstance = cfg.GetSection(configType.Name).Get<T>(bo => bo.BindNonPublicProperties = true);
                }
            }

            // in case it was not found in configuration, missing file, or simply not wanted
            // so it will take default
            if (cfgInstance == default)
            {
                // null so make new one
                cfgInstance = (T?)Activator.CreateInstance(configType);
            }

            if (cfgInstance != default)
            {
                // register config instance in autofac
                if (configType.GetInterfaces().Any())
                {
                    builder.RegisterInstance(cfgInstance).AsImplementedInterfaces();
                }

                builder.RegisterInstance(cfgInstance).AsSelf();
            }

            return cfgInstance;
        }
    }
}