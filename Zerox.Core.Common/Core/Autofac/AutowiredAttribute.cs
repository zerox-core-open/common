﻿using System;

namespace Zerox.Core.Autofac
{
    /// <summary>
    /// It is possible to auto-wire any field during IoC creation process
    /// This will happen just after constructor is called, and just before PostAutowire will be
    /// called.
    /// <br/><b><color value="#ff0000">PAY ATTENTION!</color></b> In order to have auto-wired fields defined in base classes, those must be <b>protected</b>!
    /// Otherwise they will not be found and not auto-wired
    /// </summary>
    public class AutowiredAttribute : Attribute
    {
    }
}