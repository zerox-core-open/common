﻿namespace Zerox.Core.Autofac
{
    public interface IAutowirable
    {
        void PostAutowire();
    }
}