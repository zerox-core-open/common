﻿using Autofac;

namespace Zerox.Core.Autofac
{
    public class LifetimeScopeWrapper
    {
        public ILifetimeScope? AutofacContainer { get; set; }
    }
}