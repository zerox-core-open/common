﻿using Autofac;
using Autofac.Builder;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Zerox.Core.App;

namespace Zerox.Core.Autofac
{

    public static class AutofacExtensionsBase
    {
        // key for parsed settings file saved in autofac context properties
        public static string APPSETTINGS_PROPERTY_KEY = "ConfigRoot";

        public static string[] TryToGetAutofacPrefixes(Type dest)
        {
            HashSet<string> prefixes = new HashSet<string>
            {
                "Zerox."
            };

            var dnAttribute = dest?.GetCustomAttributes(typeof(AutofacModulePrefixesAttribute), true).FirstOrDefault() as AutofacModulePrefixesAttribute;
            if (dnAttribute != default)
            {
                foreach (var s in dnAttribute.ScanPrefixes)
                {
                    if (prefixes.Where(p => p.StartsWith(s)).Count() == 0)
                    {
                        prefixes.Add(s);
                    }
                }
            }

            return prefixes.ToArray();
        }

        public static ContainerBuilder RegisterConfigFile( ContainerBuilder builder, IConfiguration cfg)
        {
            // register whole config itself
            builder.RegisterInstance(cfg).As<IConfiguration>();

            /**********************************************/
            /*  Register application configuration        */
            /**********************************************/
            builder.Properties.Add(APPSETTINGS_PROPERTY_KEY, cfg);
            return builder;
        }

        public static ContainerBuilder RegisterCustoms(ContainerBuilder builder, Action<ContainerBuilder> callBack)
        {
            callBack(builder);
            return builder;
        }

        public static ContainerBuilder RegisterModules(ContainerBuilder builder, string[] assemblyPrefixes, Action<ContainerBuilder, Assembly[]>? customRegister = null)
        {
            var assemblies = GetAssembliesFromApplicationBaseDirectory(assemblyPrefixes);

            /**********************************************/
            /*  Register all application assembly modules */
            /**********************************************/
            builder.RegisterAssemblyModules(assemblies);

            //if (doAutomapperProfiles)
            //{
            //    // here you have to pass in the assembly (or assemblies) containing AutoMapper types
            //    // stuff like profiles, resolvers and type-converters will be added by this function
            //    foreach (var ass in assemblies)
            //    {
            //        builder.RegisterAutoMapper(true, assemblies);
            //    }
            //}

            if(customRegister != null)
            {
                customRegister(builder, assemblies);
            }

            return builder;
        }

        /// <summary>
        /// This action have to be set by caller, due to imposibility to make it in netstandard
        /// it can have two different forms:
        /// Framework version: return Assembly.LoadFrom(p);
        /// or
        /// Core version: System.Runtime.Loader.AssemblyLoadContext.Default.LoadFromAssemblyPath(p);
        /// </summary>
        public static Func<string, Assembly>? AssemblyLoader { get; set; } = null;

        public static Assembly[] GetAssembliesFromApplicationBaseDirectory(string[] assemblyPrefixes)
        {
            List<string> skipTexts = new List<string>() { "test", "Test", "XUnit" };

            var loaded = AppDomain.CurrentDomain.GetAssemblies().Where(a => assemblyPrefixes.Any(p => a.FullName?.StartsWith(p) ?? false)).ToArray();

            HashSet<string> index = new HashSet<string>(loaded.Select(a => a.Location));

            // get all dlls, and load what was not loaded yet
            string baseDirectoryPath = AppExtensions.GetAppWorkingDir();

            var dlls = Directory.GetFiles(baseDirectoryPath)
                .Where((file) => assemblyPrefixes.Any(p => Path.GetFileName(file).StartsWith(p)) && string.Equals(Path.GetExtension(file), ".dll", StringComparison.OrdinalIgnoreCase))
                .Select(p =>
                {
                    try
                    {
                        if (index.Add(p))
                        {
                            
                            // this is not working on .Net Core project see: http://codebuckets.com/2020/05/29/dynamically-loading-assemblies-for-dependency-injection-in-net-core/
                            // return Assembly.LoadFrom(p);
                            // return System.Runtime.Loader.AssemblyLoadContext.Default.LoadFromAssemblyPath(p);
                            if(AssemblyLoader != null)
                            {
                                return AssemblyLoader(p);
                            }
                        }
                        return null;
                    }
                    catch (BadImageFormatException)
                    {
                        return null;
                    }
                })
                .Where(a => a != null)
                .ToArray();
            var final = AppDomain.CurrentDomain.GetAssemblies().Where(a => assemblyPrefixes.Any(p => a.FullName?.StartsWith(p) ?? false) && !skipTexts.Any(s => a.FullName.Contains(s))).ToArray();

            return final;
        }

        public static void InjectFields(IComponentContext context, object? instance, bool overrideSetValues)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            IEnumerable<FieldInfo> piR = instance.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                                                .Where(ø => ø.IsDefined(typeof(AutowiredAttribute), false));

            foreach (var propertyInfo in piR)
            {
                string n = propertyInfo.Name;
                if(n == "_scope")
                {
                    int i = 0;
                }
                Type propertyType = propertyInfo.FieldType;

                if ((!propertyType.IsValueType || propertyType.IsEnum) && context.IsRegistered(propertyType))
                {
                    // set only if empty !!!
                    if (propertyInfo.GetValue(instance) == default)
                    {
                        var obj = context.Resolve(propertyType);
                        propertyInfo.SetValue(instance, obj);
                    }
                }
                else if (!context.IsRegistered(propertyType))
                {
                    throw new ApplicationException(string.Format("Autowire failed: Missing [{0}]", propertyType.Name));
                }
                else
                {
                    throw new ApplicationException(string.Format("Autowire failed: Field [{0}] is not autowirable", propertyInfo.Name));
                }
            }

            // do post Autowire if present
            if (instance is IAutowirable tmp)
            {
                tmp.PostAutowire();
            }
        }

        public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> FieldsAsAutowired<TLimit, TActivatorData, TRegistrationStyle>(IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registration)
        {
            return registration.OnActivated(args => InjectFields(args.Context, args.Instance, true));
        }
    }
}