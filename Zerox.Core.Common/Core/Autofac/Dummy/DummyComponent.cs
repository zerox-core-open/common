﻿using Microsoft.Extensions.Logging;

namespace Zerox.Core.Autofac.Dummy
{
    public interface ISomeSvc
    {
        string GetText();
    }

    public class WillBeChangedForTestSvc : ISomeSvc, IAutowirable
    {
        public void PostAutowire()
        {
        }

        public string GetText()
        {
            return GetType().Name;
        }
    }

    public interface IDummyComponent
    {
        string? GetText();

        bool Autowired { get; }
    }

    public class DummyComponent : IDummyComponent, IAutowirable
    {
        [Autowired]
        private readonly ISomeSvc? _someSvc = default;

        public bool Autowired { get; set; } = false;

        public ILogger Logger { get; private set; }

        public DummyComponent(ILogger<DummyComponent> logger)
        {
            Logger = logger;
        }

        public string? GetText()
        {
            Logger.LogDebug("Some testing log");
            return _someSvc?.GetText();
        }

        public void PostAutowire()
        {
            Autowired = true;
        }
    }
}