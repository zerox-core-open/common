﻿using System;

namespace Zerox.Core.Autofac
{
    public class AutofacModulePrefixesAttribute : Attribute
    {
        public AutofacModulePrefixesAttribute(params string[] scanPrefixes)
        {
            ScanPrefixes = scanPrefixes;
        }

        public string[] ScanPrefixes { get; set; }
    }
}