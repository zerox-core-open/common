﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace Zerox.Core.Auth
{
    public interface IPassword
    {
        bool Verify(string password);
        void Reset(string password);
    }

    public class SecuredPassword : IPassword
    {
        private const int _saltSize = 256;
        private byte[]? _hash;
        private byte[]? _salt;

        public byte[]? Hash
        {
            get { return _hash; }
        }

        public byte[]? Salt
        {
            get { return _salt; }
        }

        // first ctor will be used for bson deserialisation
        public SecuredPassword(byte[]? Hash, byte[]? Salt)
        {
            _hash = Hash;
            _salt = Salt;
        }

        // factory
        public static SecuredPassword FromPlainPassword(string? plainPassword)
        {
            var ret = new SecuredPassword(default, default);
            ret.Reset(plainPassword);
            return ret;
        }

        public bool Verify(string? password)
        {
            if (string.IsNullOrWhiteSpace(password))
                return false;

            using (var deriveBytes = new Rfc2898DeriveBytes(password, _salt))
            {
                byte[] newKey = deriveBytes.GetBytes(_saltSize);

                return newKey.SequenceEqual(_hash);
            }
        }

        public void Reset(string? plainPassword)
        {
            if (string.IsNullOrWhiteSpace(plainPassword))
                return;

            using (var deriveBytes = new Rfc2898DeriveBytes(plainPassword, _saltSize))
            {
                _salt = deriveBytes.Salt;
                _hash = deriveBytes.GetBytes(_saltSize);
            }
        }
    }
}
