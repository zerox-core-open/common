﻿using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zerox.Core.Auth
{
    public interface IRight
    {
        public string Value { get; }
    }

    public interface IUser<IDT>
    {
        public IDT Id { get; }
        public string Login { get; }
        public IEnumerable<IRight>? Rights { get; }
    }

    public interface IAuthRequest
    {
        public string Login { get; }
        public string Password { get; }
    }

    public interface IAuthResponse<IDT>
    {
        public IUser<IDT> User { get; }
        public string Token { get; }
    }

    public interface IUserSource<IDT>
    {
        Task<IUser<IDT>?> LoginAsync(string login, string password);
        Task<IUser<IDT>?> GetByIdAsync(IDT id);
        Task<IEnumerable<string>> GetUsersAsync();
    }

    public interface IAuthContext<IDT>
    {
        Task<IAuthResponse<IDT>?> AuthenticateAsync(IAuthRequest req);

        Task<IUser<IDT>?> TryValidateTokenAsync(string? token);
        
        Task<IEnumerable<string>> GetAllUsersAsync();
        
    }

    public interface ISecurityContext<IDT>
    {
        IUser<IDT>? CurrentUser { get; }
        void Reset();
    }


    public interface IAuthTokenService<IDT>
    {
        string GenerateToken(IUser<IDT> user);
        string GenerateToken(IUser<IDT> user, TimeSpan duration);
        SecurityToken? ValidateToken(string? token);
    }

    public interface IUserWithPassword<PwdT, IDT> : IUser<IDT>
        where PwdT : IPassword
    {
        //security
        PwdT Pwd { get; }
    }

}
