﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;


namespace Zerox.Core.Reflection
{
   /// <summary>
   /// This type of property accesor use delegate, it is slightly slower vs Compiled linq expression due to one 
   /// cast necessary cause delegate must be strongly typed
   /// </summary>
   public abstract class Accessor
   {
      public abstract void MakeAccessors(PropertyInfo pi);
      public abstract object? Get(object obj);
      public abstract void Set(object obj, object value);
   }
   

   public class PropAccessor<T1, T2> : Accessor
   {
      private Func<T1, T2>? _get;
      private Action<T1, T2>? _set;

      public override object? Get(object obj) => _get != null ? _get((T1)obj) : null;
      public override void Set(object obj, object value) {
            if (_set != null)
            {
                _set((T1)obj, (T2)value);
            }
      }

      public PropAccessor() { }

      public override void MakeAccessors(PropertyInfo pi)
      {
         _get = (Func<T1, T2>)Delegate.CreateDelegate(typeof(Func<T1, T2>), null, pi.GetMethod);
         _set = (Action<T1, T2>)Delegate.CreateDelegate(typeof(Action<T1, T2>), null, pi.SetMethod);
      }
   }

   /// <summary>
   /// Fast property wrapper, it wraps some property with compiled expression for gettr and setter, due to its class nature it can be cached
   /// and so we can have some sort of very fast reflection. (anyway reflection was optimised in last versions of c#, but Fast property is still very handy.
   /// </summary>
   public class FastProperty : IFastProperty
   {
      /// <summary>
      /// if true it will handle only public property accessors.
      /// </summary>
      private bool _strict;

      /// <summary>
      /// Cache original property info
      /// </summary>
      /// <value>The property.</value>
      public PropertyInfo Property { get; protected set; }

      /// <summary>
      /// Gets or sets the type of the parameter being passed in as the instance.
      /// </summary>
      /// <value>The type of the instance.</value>
      public Type InstanceType { get; private set; }

      /// <summary>
      /// Gets or sets the type of value being returned by the property.
      /// </summary>
      /// <value>The type of the return.</value>
      public Type ReturnType { get; private set; }

      /// <summary>
      /// Gets a value indicating whether this instance can read.
      /// </summary>
      /// <value><c>true</c> if this instance can read; otherwise, <c>false</c>.</value>
      public bool CanRead
      {
         get
         {
            return Property.CanRead;
         }
      }

      /// <summary>
      /// Gets a value indicating whether this instance can write.
      /// </summary>
      /// <value><c>true</c> if this instance can write; otherwise, <c>false</c>.</value>
      public bool CanWrite
      {
         get
         {
            return Property.CanWrite;
         }
      }

#if USE_ACCESSOR
        /// used for delegate accessor (mostly only for tests and pocs
        private Accessor _accessor;
#endif

      /// <summary>
      /// Makes the fast property for the specified <see cref="System.Reflection.PropertyInfo">Property</see>.
      /// </summary>
      /// <param name="property">The property.</param>
      /// <param name="strict">If true only public access</param>
      /// <returns></returns>
      public static IFastProperty Make(PropertyInfo property, bool strict)
      {
         IFastProperty? ret;
         if (_Cache.TryGetValue(property, out ret))
         {
            return ret;
         }
         else
         {
            var returnType = typeof(object);

            // make sure that (P)value is valid
            if (!returnType.IsAssignableFrom(property.PropertyType))
            {
               throw new Exception($"The type {property.PropertyType.Name} cannot be assigned to values of type {typeof(object).Name}");
            }

            // create our fast property instance
            IFastProperty fastProperty = new FastProperty(property, strict);

            // add it to the cache
            _Cache.TryAdd(property, fastProperty);

            return fastProperty;
         }
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="FastProperty&lt;T, P&gt;"/> class.
      /// </summary>
      /// <param name="property">The property.</param>
      private FastProperty(PropertyInfo property, bool strict)
      {
         _strict = strict;
         Property = property;
         InstanceType = typeof(object);
         ReturnType = typeof(object);
         

#if USE_ACCESSOR
            MakeDelegatesAccessor();
#else
         MakeDelegates();
#endif
      }

#if USE_ACCESSOR
        private void MakeDelegatesAccessor()
        {
            Type accessorType = typeof(PropAccessor<,>).MakeGenericType(Property.DeclaringType, Property.PropertyType);
            _accessor = (Accessor)Activator.CreateInstance(accessorType);
            _accessor.MakeAccessors(Property);
        }
#endif

      /// <summary>
      /// Makes the  get/set delegates.
      /// </summary>
      private void MakeDelegates()
      {
         var instance = Expression.Parameter(InstanceType, "instance");
         var value = Expression.Parameter(ReturnType, "value");

         Expression? getExpression = null;
         Expression? setExpression;
         Expression? instanceExpression = null;
         Expression? valueExpression;

         if (Property.DeclaringType != null)
         {
            // handle instance conversion
            if (InstanceType == Property.DeclaringType)
            {
               instanceExpression = instance;
            }
            else
            {
               instanceExpression = Property.DeclaringType.IsValueType ?
                   Expression.Convert(instance, Property.DeclaringType) :
                   Expression.TypeAs(instance, Property.DeclaringType);
            }
         }

         // create the call to the get method
         if (Property.CanRead && instanceExpression != null)
         {
            var mi = Property.GetGetMethod(!_strict);
            if (mi != null)
            {
               getExpression = Expression.Call(instanceExpression, mi);
            }
         }

         // handle value conversion
         if (ReturnType == Property.PropertyType)
         {
            valueExpression = value;
         }
         else
         {
            if (Property.PropertyType.IsValueType)
            {
               valueExpression = Expression.Convert(value, Property.PropertyType);

               // convert the return value of the get method
               if (getExpression != null)
               {
                  getExpression = Expression.Convert(getExpression, ReturnType);
               }
            }
            else
            {
               valueExpression = Expression.TypeAs(value, Property.PropertyType);

               // convert the return value of the get method
               if (getExpression != null)
               {
                  getExpression = Expression.TypeAs(getExpression, ReturnType);
               }
            }
         }

         if (Property.CanRead && getExpression != default)
         {
            _Get = Expression.Lambda<Func<object, object?>>(getExpression, instance).Compile();
         }

         if (Property.CanWrite)
         {
            // create the call to the set method
            MethodInfo? mi = Property.GetSetMethod(!_strict);
            if (mi != null && instanceExpression != null && valueExpression != null)
            {
               setExpression = Expression.Call(instanceExpression, mi, valueExpression);
               _Set = Expression.Lambda<Action<object, object?>>(setExpression, instance, value).Compile();
            }
         }
      }

      private Func<object, object?> _Get = (o) => null;
      /// <summary>
      /// Gets or sets the get.
      /// </summary>
      /// <value>The get.</value>
      public Func<object, object?> Get => _Get;

      private Action<object, object?> _Set = (o, v) => { };
      /// <summary>
      /// Gets or sets the set.
      /// </summary>
      /// <value>The set.</value>
      public Action<object, object?> Set => _Set;

      /// <summary>
      /// A cache to store properties that have already been parsed
      /// </summary>
      private static readonly ConcurrentDictionary<PropertyInfo, IFastProperty> _Cache = new ConcurrentDictionary<PropertyInfo, IFastProperty>();

      /// <summary>
      /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
      /// <returns>
      /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
      /// </returns>
      /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
      public override bool Equals(object? obj)
      {
         if (obj == null)
         {
            throw new NullReferenceException();
         }

         // check to see if we are comparing fast properties
         if (obj is IFastProperty instance)
         {
            return (
                Property == instance.Property &&
                ReturnType == instance.ReturnType &&
                InstanceType == instance.InstanceType
            );
         }

         return false;
      }

      /// <summary>
      /// Serves as a hash function for a particular type.
      /// </summary>
      /// <returns>
      /// A hash code for the current <see cref="T:System.Object"/>.
      /// </returns>
      public override int GetHashCode()
      {
         return Property.GetHashCode();
      }

      /// <summary>
      /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <returns>
      /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
      /// </returns>
      public override string ToString()
      {
         return string.Format("FastProperty<{0},{1}> - {2}.{3}", InstanceType.Name, ReturnType.Name, Property.DeclaringType?.Name, Property.Name);
      }


#if USE_ACCESSOR
        object? IFastProperty.GetVal(object obj) => _accessor.Get(obj);

        public void SetVal(object obj, object? value) => _accessor.Set(obj, value);
#else
      object? IFastProperty.GetVal(object obj) => _Get(obj);

      public void SetVal(object obj, object? value) => _Set(obj, value);
#endif

      
   }
}