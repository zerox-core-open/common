﻿using K4os.Compression.LZ4.Streams;
using MimeMapping;
using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Zerox.Core.Support;

namespace Zerox.Core.Io
{
    public interface ICompressionExtendedInfo<KindT> where KindT : struct, Enum
    {
        string CompressedExt { get; set; }
        string CompressedHash { get; set; }
        string CompressedMime { get; set; }
        long CompressedSize { get; set; }
        string Description { get; set; }
        string FileExtension { get; set; }
        string FileFullName { get; set; }
        string Hash { get; set; }
        KindT Kind { get; set; }
        DateTime LastWrite { get; set; }
        string Mime { get; set; }
        string NameWithoutExt { get; set; }
        long Size { get; set; }
    }

    [Serializable, JsonObject]
    public class CompressionExtendedInfo<KindT> : ICompressionExtendedInfo<KindT> where KindT : struct, Enum
    {
        public DateTime LastWrite { get; set; }
        public string Hash { get; set; } = string.Empty;
        public string Mime { get; set; } = string.Empty;
        public long Size { get; set; } = 0;
        public string CompressedHash { get; set; } = string.Empty;
        public string CompressedMime { get; set; } = string.Empty;
        public string CompressedExt { get; set; } = string.Empty;
        public long CompressedSize { get; set; } = 0;
        public string NameWithoutExt { get; set; } = string.Empty;
        public string FileExtension { get; set; } = string.Empty;
        public string FileFullName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public KindT Kind { get; set; } = (KindT)Enum.ToObject(typeof(KindT), 0);
    }

    public class CompressedStreamWithInfo<ExtInfoT, KindT> : IDisposable
        where ExtInfoT : ICompressionExtendedInfo<KindT>, new()
        where KindT : struct, Enum
    {
        public Stream Stream { get; }
        public ExtInfoT Info { get; }

        public CompressedStreamWithInfo(Stream stream, ExtInfoT info)
        {
            Stream = stream;
            Info = info;
        }

        #region Dispose
        private bool disposedValue;

        

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Stream.Dispose();
                }
                // TODO: liberare risorse non gestite (oggetti non gestiti) ed eseguire l'override del finalizzatore
                // TODO: impostare campi di grandi dimensioni su Null
                disposedValue = true;
            }
        }

        // // TODO: eseguire l'override del finalizzatore solo se 'Dispose(bool disposing)' contiene codice per liberare risorse non gestite
        // ~CompressedStreamWithInfo()
        // {
        //     // Non modificare questo codice. Inserire il codice di pulizia nel metodo 'Dispose(bool disposing)'
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Non modificare questo codice. Inserire il codice di pulizia nel metodo 'Dispose(bool disposing)'
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }

    public static class FileExtensions
    {
        public enum CompressionMethodEnum
        {
            Deflate = 0,
            Lz4,
            Gzip
        }

        public static string ToMime(this string path)
        {
            if (path.ToLower().EndsWith(".lz4"))
            {
                return "application/lz4";
            }
            if (path.ToLower().EndsWith(".gz"))
            {
                return "application/gzip";
            }
            if (path.ToLower().EndsWith(".dfl"))
            {
                return "application/dfl";
            }
            return MimeUtility.GetMimeMapping(path);
        }

        public static CompressionMethodEnum ToCompressionMethod(this string path)
        {
            if (path.ToLower().EndsWith(".gz"))
            {
                return CompressionMethodEnum.Gzip;
            }
            if (path.ToLower().EndsWith(".lz4"))
            {
                return CompressionMethodEnum.Lz4;
            }
            return CompressionMethodEnum.Deflate;
        }

        public static string ToExtension(this CompressionMethodEnum method)
        {
            return method == CompressionMethodEnum.Deflate ? ".dfl" :
                method == CompressionMethodEnum.Lz4 ? ".lz4" : ".gz";
        }

        public static string ToMime(this CompressionMethodEnum method)
        {
            return method == CompressionMethodEnum.Deflate ? "application/dfl" :
                method == CompressionMethodEnum.Lz4 ? "application/lz4" : "application/gzip";
        }

        public static Stream GetDecompressStream(this Stream s, CompressionMethodEnum method = CompressionMethodEnum.Deflate, bool leaveOpen = false)
        {
            switch (method)
            {
                default:
                case CompressionMethodEnum.Deflate:
                    return new DeflateStream(s, CompressionMode.Decompress, leaveOpen);
                case CompressionMethodEnum.Lz4:
                    return LZ4Stream.Decode(s, null, leaveOpen);
                case CompressionMethodEnum.Gzip:
                    return new GZipStream(s, CompressionMode.Decompress, leaveOpen);
            }
        }

        public static Stream GetCompressStream(this Stream s, CompressionMethodEnum method = CompressionMethodEnum.Deflate, bool leaveOpen = false)
        {
            switch (method)
            {
                default:
                case CompressionMethodEnum.Deflate:
                    return new DeflateStream(s, CompressionMode.Compress, leaveOpen);
                case CompressionMethodEnum.Lz4:
                    return LZ4Stream.Encode(s, null, leaveOpen);
                case CompressionMethodEnum.Gzip:
                    return new GZipStream(s, CompressionMode.Compress, leaveOpen);
            }
        }

        public static async Task<CompressedStreamWithInfo<ExtInfoT, KindT>> CompressInMemoryStreamAsync<ExtInfoT, KindT>(
            this FileInfo f, 
            KindT kind, 
            CompressionMethodEnum method = CompressionMethodEnum.Deflate, 
            string descriptionPath = "", 
            Action<ExtInfoT>? setup = default
        )
            where ExtInfoT : ICompressionExtendedInfo<KindT>, new()
            where KindT : struct, Enum
        {
            using var oHasher = MD5.Create(); // hasher for original file
            using var cHasher = MD5.Create(); // hasher for compressed stream
            MemoryStream result = new MemoryStream(); // this will remain open and must be disposed by caller !!!!
            using (LeaveItOpenStreamWrapper outStream = new LeaveItOpenStreamWrapper(result))
            {
                // last true is cause we need that uderline <outStream> stream will be not closed !!!
                using (CryptoStream cHashStrm = new CryptoStream(outStream, cHasher, CryptoStreamMode.Write))
                {
                    long origSize = 0;
                    // compressor
                    using (Stream comprStrm = cHashStrm.GetCompressStream(method))
                    {
                        // original file hash
                        using (CryptoStream oHashStrm = new CryptoStream(comprStrm, oHasher, CryptoStreamMode.Write))
                        {
                            // source file
                            using (FileStream source = File.OpenRead(f.FullName))
                            {
                                await source.CopyToAsync(oHashStrm);
                                origSize = source.Position;
                            }
                        }
                    }
                    var comprSize = outStream.Position;


                    // go back
                    outStream.Position = 0;
                    if (cHasher.Hash != default && oHasher.Hash != default)
                    {
                        var oMime = f.FullName.ToMime();
                        IMetaExtractor ext = MetaExtractorContext.GetExtractor(oMime);
                        var info = new ExtInfoT()
                        {
                            LastWrite = f.LastWriteTime,
                            FileExtension = Path.GetExtension(f.FullName),
                            FileFullName = f.FullName,
                            NameWithoutExt = Path.GetFileNameWithoutExtension(f.FullName),
                            Mime = oMime,
                            CompressedMime = method.ToMime(),
                            CompressedHash = BitConverter.ToString(cHasher.Hash).Replace("-", "").ToLowerInvariant(),
                            CompressedExt = method.ToExtension(),
                            Hash = BitConverter.ToString(oHasher.Hash).Replace("-", "").ToLowerInvariant(),
                            Size = origSize,
                            CompressedSize = comprSize,
                            Description = string.IsNullOrEmpty(descriptionPath) ? string.Empty : ext.GetMetaByPath(f.FullName, descriptionPath) ?? string.Empty,
                            Kind = kind
                        };

                        if(setup != default)
                        {
                            setup(info);
                        }

                        return new CompressedStreamWithInfo<ExtInfoT, KindT>(result, info);
                    }

                    throw new ApplicationException("Problem generating MD5 hash");

                }
            }
        }

        public class FileExtendedInfo {
            public FileExtendedInfo(DateTime lastWrite, string md5, string mime, long size, string ext, string nameWithoutExt)
            {
                LastWrite = lastWrite;
                Md5 = md5;
                Mime = mime;
                Size = size;
                Ext = ext;
                NameWithoutExt = nameWithoutExt;
            }

            public DateTime LastWrite { get; set; }
            public string Md5 { get; set; }
            public string Mime { get; set; }
            public long Size { get; set; }
            public string Ext { get; set; }
            public string NameWithoutExt { get; set; }
        }

        public static FileExtendedInfo ExtractInfo(this FileInfo f)
        {
            using (var strm = File.OpenRead(f.FullName)) {
                return new FileExtendedInfo(
                    File.GetLastWriteTime(f.FullName),
                    strm.ToMD5(),
                    f.FullName.ToMime(),
                    f.Length,
                    f.Extension,
                    Path.GetFileNameWithoutExtension(f.FullName)
                );
            }
        }
    }
}
