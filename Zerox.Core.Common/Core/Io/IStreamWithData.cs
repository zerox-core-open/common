﻿using System.IO;

namespace Zerox.Core.Io
{
    public interface IStreamWithData<DataT>
        where DataT : class, new()
    {
        DataT? Data { get; }
        Stream? Stream { get; }
    }
}
