﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Zerox.Core.CSharp
{
    public class DynAssembly
    {
        private readonly ILogger<DynAssembly> _logger;

        public DynAssembly(ILogger<DynAssembly> l)
        {
            _logger = l;
        }

        public class ReferenceBuilder
        {
            private List<MetadataReference> _refs = new List<MetadataReference>();

            internal ReferenceBuilder()
            {
                var referencedAssemblies = Assembly.GetExecutingAssembly().GetReferencedAssemblies();
                _refs.AddRange(referencedAssemblies.Select(a => MetadataReference.CreateFromFile(Assembly.Load(a).Location)));
                _refs.Add(MetadataReference.CreateFromFile(typeof(object).Assembly.Location));
                _refs.Add(MetadataReference.CreateFromFile(Assembly.Load("netstandard, Version=2.0.0.0").Location));
            }

            public ReferenceBuilder AddFromType<T>()
            {
                return AddFromType(typeof(T));
            }

            public ReferenceBuilder AddFromType(Type t)
            {
                _refs.Add(MetadataReference.CreateFromFile(t.GetTypeInfo().Assembly.Location));
                return this;
            }

            public List<MetadataReference> Build()
            {
                return _refs;
            }
        }

        public static ReferenceBuilder GetReferncesBuilder()
        {
            return new ReferenceBuilder();
        }

        private static string AddAssemblyInfo(string name, Version version)
        {
            string info = $@"
using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute(""{name}"")]
[assembly: System.Reflection.AssemblyCopyrightAttribute(""Copyright © 2020"")]
[assembly: System.Reflection.AssemblyDescriptionAttribute(""{name}"")]
[assembly: System.Reflection.AssemblyFileVersionAttribute(""{version}"")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute(""{version}"")]
[assembly: System.Reflection.AssemblyProductAttribute(""{name}"")]
[assembly: System.Reflection.AssemblyTitleAttribute(""{name}"")]
[assembly: System.Reflection.AssemblyVersionAttribute(""{version}"")]
";         
            return info;
        }

        private static CSharpCompilation CompileAssembly(string name, Version version, List<string> sources, Action<ReferenceBuilder> refsBuilder)
        {
            var options = CSharpParseOptions.Default.WithLanguageVersion(LanguageVersion.CSharp8);

            sources.Add(AddAssemblyInfo(name, version));
            var parsedSyntaxTrees = sources.Select(el => SyntaxFactory.ParseSyntaxTree(el, options));

            var rBuilder = GetReferncesBuilder();
            refsBuilder(rBuilder);

            return CSharpCompilation.Create($"{name}.dll",
                parsedSyntaxTrees,
                references: rBuilder.Build(),
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary,
                    concurrentBuild: true,
                    optimizationLevel: OptimizationLevel.Release,
                    assemblyIdentityComparer: DesktopAssemblyIdentityComparer.Default));
        }

        public bool BuildAssembly(string name, Version version, List<string> sources, Action<ReferenceBuilder> refsBuilder, Stream dest)
        {
            if (_logger.IsEnabled(LogLevel.Trace))
            {
                _logger.LogTrace($"Start compile [{name} ver: {version} #{sources.Count} classes]  ---------------->");
            }
            var compilation = CompileAssembly(name, version, sources, refsBuilder);
            // this resources is necessary for version info!!!
            using (Stream win32resStream = compilation.CreateDefaultWin32Resources(
                                                                versionResource: true, // Important!
                                                                noManifest: false,
                                                                manifestContents: null,
                                                                iconInIcoFormat: null))
            {
                var result = compilation.Emit(
                    dest,
                    default,
                    default,
                    win32resStream
                );

                if (!result.Success)
                {
                    var failures = result.Diagnostics
                        .Where(diagnostic => diagnostic.IsWarningAsError ||
                                             diagnostic.Severity == DiagnosticSeverity.Error);

                    _logger.LogError("Compilation problems START  ########### >");
                    foreach (var f in failures)
                    {
                        _logger.LogError(f.ToString());
                    }
                    _logger.LogError("Compilation problems END < ###########");
                    return false;
                }
            }
                

            if (_logger.IsEnabled(LogLevel.Trace))
            {
                _logger.LogTrace($"Compile done!  <----------------");
            }
            return true;
        }
    }
}