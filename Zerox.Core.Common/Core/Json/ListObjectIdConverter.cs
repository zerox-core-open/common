﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

#nullable disable

namespace Zerox.Core.Json
{
    public class ListObjectIdConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(List<ObjectId>);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return new List<ObjectId>() { ObjectId.Empty };

            if (reader.TokenType != JsonToken.StartArray)
                throw new Exception($"Unexpected token parsing List<ObjectId>. Expected StartArray, got {reader.TokenType}.");


            var list = new List<ObjectId>();
            while (reader.Read())
            {
                if (reader.Value != null && reader.TokenType == JsonToken.String)
                {
                    var value = (string)reader.Value;
                    list.Add(string.IsNullOrEmpty(value) ? ObjectId.Empty : new ObjectId(value));
                }
                else
                {
                    break;
                }
            }
            return list;
        }

        public override void WriteJson(JsonWriter writer, object listValues, JsonSerializer serializer)
        {
            if (listValues is List<ObjectId> list)
            {
                if(list.Count <= 0)
                {
                    writer.WriteValue(string.Empty);
                    return;
                }

                writer.WriteStartArray();
                foreach (var value in list)
                {
                    var objectId = (ObjectId)value;
                    writer.WriteValue(objectId != ObjectId.Empty ? objectId.ToString() : string.Empty);
                }
                writer.WriteEndArray();
            }
            else
            {
                throw new Exception("Expected ObjectId value.");
            }
        }
    }
}
