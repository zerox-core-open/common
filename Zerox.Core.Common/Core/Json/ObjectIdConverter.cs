﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.ComponentModel;

#nullable disable

namespace Zerox.Core.Json
{

    /// <summary>
    /// DevExtreme.AspNet.Data.Utils when do conversion of Expression need eventual converter if property type is ObjectId
    /// it will ask for TypeDescriptor converter, this must be register into TypeDescriptor in some program early run phase with this line:
    /// <code>
    /// TypeDescriptor.AddAttributes(typeof(ObjectId), new TypeConverterAttribute(typeof(ExpressionTypeObjectIdConverter)));
    /// </code>
    /// </summary>
    public class ExpressionTypeObjectIdConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext? context, Type sourceType)
        {
            return sourceType == typeof(string) || sourceType == typeof(int);
        }
        public override bool CanConvertTo(ITypeDescriptorContext? context, Type? destinationType)
        {
            return destinationType == typeof(string) || destinationType == typeof(int);
        }
        public override object ConvertFrom(ITypeDescriptorContext? context, System.Globalization.CultureInfo? culture, object value)
        {
            if (value is string str)
            {
                return ObjectId.Parse(str);
            }
            else if (value is int i)
            {
                return ObjectId.Parse(i.ToString("D24"));
            }

            return value;
        }
        public override object ConvertTo(ITypeDescriptorContext? context, System.Globalization.CultureInfo? culture, object? value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is ObjectId obj)
            {
                return obj.ToString();
            }
            return "0000000000000000000000";
        }
    }

    public class ObjectIdConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ObjectId);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return ObjectId.Empty;

            if (reader.TokenType != JsonToken.String)
                throw new Exception($"Unexpected token parsing ObjectId. Expected String, got {reader.TokenType}.");

            var value = (string)reader.Value;
            return string.IsNullOrEmpty(value) ? ObjectId.Empty : new ObjectId(value);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is ObjectId)
            {
                var objectId = (ObjectId)value;
                writer.WriteValue(objectId != ObjectId.Empty ? objectId.ToString() : string.Empty);
            }
            else
            {
                throw new Exception("Expected ObjectId value.");
            }
        }
    }
}
