﻿using System;

namespace Zerox.Core.Common.Cache
{
    public class IndexAttribute : Attribute
    {

        public bool IsMain { get; set; } = false;
    }
}
